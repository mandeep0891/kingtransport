package com.kingtransport.tools;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Validation utility for Collection, Map, and Object .
 * 
 * @author Mandeep Singh Gill
 *
 */
public class ToolBox {

	/**
	 * Validation for any kind of collection. e.g set,queue,list
	 * 
	 * @param obj
	 * @return
	 */
	public static <T> boolean isCollectionEmpty(Collection<T> obj) {
		return null == obj || obj.isEmpty();
	}

	/**
	 * Validation for Map,HashTable like as collection
	 * 
	 * @param obj
	 * @return
	 */
	public static <T, V> boolean isCollectionEmpty(Map<T, V> obj) {
		return null == obj || obj.isEmpty();
	}

	/**
	 * validation for Object type.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isObjectEmpty(Object obj) {
		return null == obj || obj.toString().trim().isEmpty();
	}

	/*
	 * public static void main(String[] args) { List<String> lists = new
	 * ArrayList<String>(); lists.add("dfgfdgdfg");
	 * System.out.println(isCollectionEmpty(lists)); }
	 */

	public static boolean isEmptyString(String str) {
		boolean status = false;
		if (str == null || str.isEmpty()) {
			return true;
		}
		return status;
	}

	/**
	 * Checks if given object is not null.
	 *
	 * @param object the object
	 * @return true, if is not null
	 */
	public static boolean isNotNull(Object object) {
		return Objects.nonNull(object);
	}

	/**
	 * Checks if given object is null.
	 *
	 * @param object the object
	 * @return true, if is null
	 */
	public static boolean isNull(Object object) {
		return Objects.isNull(object);
	}

}
