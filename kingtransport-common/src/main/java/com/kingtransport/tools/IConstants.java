/**
 * 
 */
package com.kingtransport.tools;

/**
 * @author Mandeep Singh
 *
 */
public class IConstants {

	public static final String ACTIVE = "ACTIVE";
	public static final String INACTIVE = "INACTIVE";

	public static final String SUCCESS_MESSAGE = "Saved successfully";
	public static final String FETCHED_SUCCESSFULLY = "Records fetched scuccessfully.";
	public static final String ACCESS_DENIED = "You don't have access to do this.";

	public enum Role {
		FOUNDER_ROLE(1l, "founder"), ADMIN_ROLE(2l, "admin"), EMPLYEE_ROLE(3l, "employee"), DRIVER_ROLE(4l, "driver");

		private Long key;
		private String value;

		/**
		 * @param key
		 * @param value
		 */
		private Role(Long key, String value) {
			this.key = key;
			this.value = value;
		}

		/**
		 * @return the key
		 */
		public Long getKey() {
			return key;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

	}

	public interface PageName {
		public static final String login = "login";
	}

	public interface ColumnLength {
		public static final int TINY_TEXT = 20;
		public static final int SHORT_TEXT = 80;
		public static final int MEDIUM_TEXT = 255;
		public static final int LONG_TEXT = 1000;
		public static final int LARGE_TEXT = 4000;
	}

	public interface EmailTempate {
		public static final Long EMAIL_TO_DISPATCH = 10000001L;
	}

	public enum MailTemplateParams {
		USER_NAME("username"), FIRST_NAME("firstName"), LAST_NAME("lastName");

		private String key;

		/**
		 * @param key
		 */
		private MailTemplateParams(String key) {
			this.key = key;
		}

		/**
		 * @return the key
		 */
		public String getKey() {
			return key;
		}
	}

	public enum MailTemplate {
		RESET_PASSWORD(10000002L, "Reset Password"), OTP_MAIL(2l, "One time password"), LOGIN(3l, "Login"),
		SIGNUP(4l, "Signup");

		private Long key;
		private String value;

		/**
		 * @param key
		 */
		private MailTemplate(Long key, String value) {
			this.key = key;
			this.value = value;
		}

		/**
		 * @return the key
		 */
		public Long getKey() {
			return key;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

	}
}
