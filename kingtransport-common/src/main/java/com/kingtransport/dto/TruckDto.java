package com.kingtransport.dto;

public class TruckDto {

	private Long id;
	private String name;
	private String truckNo;
	private Long companyId;
	private String companyName;

	public TruckDto(Long id, String name, String truckNo) {
		super();
		this.id = id;
		this.name = name;
		this.truckNo = truckNo;
	}

	public TruckDto(Long id, String name, String truckNo, Long companyId) {
		super();
		this.id = id;
		this.name = name;
		this.truckNo = truckNo;
		this.companyId = companyId;
	}

	public TruckDto(Long id, String name, String truckNo, Long companyId, String companyName) {
		super();
		this.id = id;
		this.name = name;
		this.truckNo = truckNo;
		this.companyId = companyId;
		this.companyName = companyName;
	}

	public TruckDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTruckNo() {
		return truckNo;
	}

	public void setTruckNo(String truckNo) {
		this.truckNo = truckNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

}
