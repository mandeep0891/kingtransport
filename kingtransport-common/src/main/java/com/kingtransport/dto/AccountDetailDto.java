package com.kingtransport.dto;

import java.util.Date;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
public class AccountDetailDto {

	private Long id;
	private Long accountId;
	private Long addressId;
	private Long companyId;
	private Long roleId;
	private Long accountRoleId;
	private Long ssNo;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String phoneNumber1;
	private long profileImage;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private Date dob;
	private String userName;
	private String companyName;
	private String roleName;

	public AccountDetailDto() {
		super();
	}

	public AccountDetailDto(Long id, String email, String phoneNumber, String companyName, String roleName) {
		super();
		this.id = id;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.companyName = companyName;
		this.roleName = roleName;
	}

	public AccountDetailDto(Long companyId, Long roleId, Long ssNo, String firstName, String lastName, String email,
			String phoneNumber, String phoneNumber1, long profileImage, String address1, String address2,
			String address3, String city, String state, String country, String postalCode, Date dob) {
		super();
		this.companyId = companyId;
		this.roleId = roleId;
		this.ssNo = ssNo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.phoneNumber1 = phoneNumber1;
		this.profileImage = profileImage;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.dob = dob;
	}
	
	
	public AccountDetailDto(Long id, Long accountId, Long addressId, Long companyId, Long roleId, Long accountRoleId,
			Long ssNo, String firstName, String lastName, String email, String phoneNumber, String phoneNumber1,
			long profileImage, String address1, String address2, String address3, String city, String state,
			String country, String postalCode, Date dob) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.addressId = addressId;
		this.companyId = companyId;
		this.roleId = roleId;
		this.accountRoleId = accountRoleId;
		this.ssNo = ssNo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.phoneNumber1 = phoneNumber1;
		this.profileImage = profileImage;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.dob = dob;
	}

	public AccountDetailDto(Long id, Long accountId, Long addressId, Long companyId, Long roleId, Long accountRoleId,
			Long ssNo, String firstName, String lastName, String email, String phoneNumber, String phoneNumber1,
			long profileImage, String address1, String address2, String address3, String city, String state,
			String country, String postalCode, Date dob, String userName, String roleName, String companyName) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.addressId = addressId;
		this.companyId = companyId;
		this.roleId = roleId;
		this.accountRoleId = accountRoleId;
		this.ssNo = ssNo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.phoneNumber1 = phoneNumber1;
		this.profileImage = profileImage;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.dob = dob;
		this.userName = userName;
		this.roleName = roleName;
		this.companyName = companyName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getSsNo() {
		return ssNo;
	}

	public void setSsNo(Long ssNo) {
		this.ssNo = ssNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public long getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(long profileImage) {
		this.profileImage = profileImage;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getAccountRoleId() {
		return accountRoleId;
	}

	public void setAccountRoleId(Long accountRoleId) {
		this.accountRoleId = accountRoleId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}