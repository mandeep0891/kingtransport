package com.kingtransport.dto;

public class TrailerDto {

	private Long id;
	private String name;
	private String trailerNo;
	private Long companyId;
	private String companyName;

	public TrailerDto(Long id, String name, String trailerNo) {
		super();
		this.id = id;
		this.name = name;
		this.trailerNo = trailerNo;
	}

	public TrailerDto(Long id, String name, String trailerNo, Long companyId) {
		super();
		this.id = id;
		this.name = name;
		this.trailerNo = trailerNo;
		this.companyId = companyId;
	}

	public TrailerDto(Long id, String name, String trailerNo, Long companyId, String companyName) {
		super();
		this.id = id;
		this.name = name;
		this.trailerNo = trailerNo;
		this.companyId = companyId;
		this.companyName = companyName;
	}

	public TrailerDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrailerNo() {
		return trailerNo;
	}

	public void setTrailerNo(String trailerNo) {
		this.trailerNo = trailerNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

}
