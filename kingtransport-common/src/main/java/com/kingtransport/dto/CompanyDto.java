package com.kingtransport.dto;

public class CompanyDto {
	private Long id;
	private Long companyTypeId;
	private Long addressId;
	private String companyType;
	private String name;
	private String taxId;
	private String mcNo;
	private String dotNo;
	private String email;
	private String contactNumber;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String country;
	private String postalCode;
	private String state;

	public CompanyDto() {
	}

	public CompanyDto(Long id, String companyType, String name, String taxId, String mcNo, String dotNo, String email,
			String contactNumber) {
		this.id = id;
		this.companyType = companyType;
		this.name = name;
		this.taxId = taxId;
		this.mcNo = mcNo;
		this.dotNo = dotNo;
		this.email = email;
		this.contactNumber = contactNumber;
	}

	public CompanyDto(Long id, String companyType, Long companyTypeId, String name, String taxId, String mcNo,
			String dotNo, String email, String contactNumber) {
		this.id = id;
		this.companyType = companyType;
		this.companyTypeId = companyTypeId;
		this.name = name;
		this.taxId = taxId;
		this.mcNo = mcNo;
		this.dotNo = dotNo;
		this.email = email;
		this.contactNumber = contactNumber;
	}

	/*
	 * R
	 */
	public CompanyDto(Long id, Long companyTypeId, Long addressId, String companyType, String name, String taxId,
			String mcNo, String dotNo, String email, String contactNumber, String address1, String address2,
			String address3, String city, String country, String postalCode, String state) {
		super();
		this.id = id;
		this.companyTypeId = companyTypeId;
		this.addressId = addressId;
		this.companyType = companyType;
		this.name = name;
		this.taxId = taxId;
		this.mcNo = mcNo;
		this.dotNo = dotNo;
		this.email = email;
		this.contactNumber = contactNumber;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.country = country;
		this.postalCode = postalCode;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyTypeId() {
		return companyTypeId;
	}

	public void setCompanyTypeId(Long companyTypeId) {
		this.companyTypeId = companyTypeId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getMcNo() {
		return mcNo;
	}

	public void setMcNo(String mcNo) {
		this.mcNo = mcNo;
	}

	public String getDotNo() {
		return dotNo;
	}

	public void setDotNo(String dotNo) {
		this.dotNo = dotNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
