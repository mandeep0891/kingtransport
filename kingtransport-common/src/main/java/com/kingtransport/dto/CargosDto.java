package com.kingtransport.dto;

public class CargosDto {
	private Long id;
	private Long companyId;
	private Long driverId;
	private Long additionalDriverId;
	private Long truckId;
	private Long trailerId;
	private int noOfCargos;
	private int totalBilableHours;
	private String companyName;
	private String driverName;
	private String additionalDriverName;
	private String truckName;
	private String trailerName;

	public CargosDto() {
		super();
	}

	public CargosDto(Long id, Long companyId, Long driverId, Long additionalDriverId, Long truckId, Long trailerId,
			int noOfCargos, int totalBilableHours, String companyName, String driverName, String additionalDriverName,
			String truckName, String trailerName) {
		super();
		this.id = id;
		this.companyId = companyId;
		this.driverId = driverId;
		this.additionalDriverId = additionalDriverId;
		this.truckId = truckId;
		this.trailerId = trailerId;
		this.noOfCargos = noOfCargos;
		this.totalBilableHours = totalBilableHours;
		this.companyName = companyName;
		this.driverName = driverName;
		this.additionalDriverName = additionalDriverName;
		this.truckName = truckName;
		this.trailerName = trailerName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public Long getAdditionalDriverId() {
		return additionalDriverId;
	}

	public void setAdditionalDriverId(Long additionalDriverId) {
		this.additionalDriverId = additionalDriverId;
	}

	public Long getTruckId() {
		return truckId;
	}

	public void setTruckId(Long truckId) {
		this.truckId = truckId;
	}

	public Long getTrailerId() {
		return trailerId;
	}

	public void setTrailerId(Long trailerId) {
		this.trailerId = trailerId;
	}

	public int getNoOfCargos() {
		return noOfCargos;
	}

	public void setNoOfCargos(int noOfCargos) {
		this.noOfCargos = noOfCargos;
	}

	public int getTotalBilableHours() {
		return totalBilableHours;
	}

	public void setTotalBilableHours(int totalBilableHours) {
		this.totalBilableHours = totalBilableHours;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getAdditionalDriverName() {
		return additionalDriverName;
	}

	public void setAdditionalDriverName(String additionalDriverName) {
		this.additionalDriverName = additionalDriverName;
	}

	public String getTruckName() {
		return truckName;
	}

	public void setTruckName(String truckName) {
		this.truckName = truckName;
	}

	public String getTrailerName() {
		return trailerName;
	}

	public void setTrailerName(String trailerName) {
		this.trailerName = trailerName;
	}

}
