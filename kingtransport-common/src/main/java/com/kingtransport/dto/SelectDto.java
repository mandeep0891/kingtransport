package com.kingtransport.dto;

public class SelectDto {

	private Long id;
	private String name;

	public SelectDto() {
		super();
	}

	public SelectDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
