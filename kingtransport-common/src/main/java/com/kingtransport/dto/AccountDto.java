package com.kingtransport.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Mandeep Singh Gill
 *
 *         Created to Transfer Account data
 */
public class AccountDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String userName;
	private String password;
	private String status;
	private List<RoleMasterDto> roles = null;

	public AccountDto(Long id, String userName, String password, String status) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.status = status;
	}

	public AccountDto(String userName, String password, String status) {
		super();
		this.userName = userName;
		this.password = password;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RoleMasterDto> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleMasterDto> roles) {
		this.roles = roles;
	}

}
