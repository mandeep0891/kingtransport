package com.kingtransport.dto;

import java.io.Serializable;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
public class RoleMasterDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8122842138622870413L;

	private Long id;

	private String roleName;

	private String description;

	private String status;
	
	public RoleMasterDto(String roleName) {
		super();
		this.roleName = roleName;
	}

	public RoleMasterDto(Long id, String roleName, String status, String description) {
		super();
		this.id = id;
		this.roleName = roleName;
		this.status = status;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
