package com.kingtransport.datatables;


import com.kingtransport.tools.ToolBox;

/**
 * This class handling attribute of Data tables and of filters
 * 
 * @author satish dhiman
 *
 */
public class DataTableAttributes {

	private Integer pageDisplayStart;

	private Integer pageDisplayLength;

	private String sortDir;

	private String sortingProperty;

	private String errorType;

	private String statusType;

	private String yearType;

	private String yearText;

	private String portfolioType;

	private boolean isAccountSelected;
	
	private boolean isAdmin;
	
	private boolean isFilterSelected;
	
	private String searchParam;
	
	public DataTableAttributes() {
	}

	public DataTableAttributes(Integer pageDisplayStart, Integer pageDisplayLength, String sortDir,
			String sortingProperty, String errorType, String statusType, String yearType, String yearText,
			String portfolioType,String searchParam) {
		this.pageDisplayStart = pageDisplayStart;
		this.pageDisplayLength = pageDisplayLength;
		this.sortDir = sortDir;
		this.sortingProperty = sortingProperty;
		this.errorType = errorType;
		this.statusType = statusType;
		this.yearType = yearType;
		this.yearText = yearText;
		this.portfolioType = portfolioType;
		this.searchParam = searchParam;
		
		if(!ToolBox.isObjectEmpty(errorType) || !ToolBox.isObjectEmpty(statusType) || !ToolBox.isObjectEmpty(yearType) || !ToolBox.isObjectEmpty(yearText) || !ToolBox.isObjectEmpty(portfolioType)){
			this.isFilterSelected = true;
		}
	}
	
	
	public DataTableAttributes(Integer pageDisplayStart, Integer pageDisplayLength, String sortDir,
			String sortingProperty,String searchParam) {
		this.pageDisplayStart = pageDisplayStart;
		this.pageDisplayLength = pageDisplayLength;
		this.sortDir = sortDir;
		this.sortingProperty = sortingProperty;
		this.searchParam = searchParam;

	}

	
	/**
	 * @return the isFilterSelected
	 */
	public boolean isFilterSelected() {
		return isFilterSelected;
	}

	/**
	 * @param isFilterSelected the isFilterSelected to set
	 */
	public void setFilterSelected(boolean isFilterSelected) {
		this.isFilterSelected = isFilterSelected;
	}

	
	
	/**
	 * @return the pageDisplayStart
	 */
	public Integer getPageDisplayStart() {
		return pageDisplayStart;
	}

	/**
	 * @param pageDisplayStart
	 *            the pageDisplayStart to set
	 */
	public void setPageDisplayStart(Integer pageDisplayStart) {
		this.pageDisplayStart = pageDisplayStart;
	}

	/**
	 * @return the pageDisplayLength
	 */
	public Integer getPageDisplayLength() {
		return pageDisplayLength;
	}

	/**
	 * @param pageDisplayLength
	 *            the pageDisplayLength to set
	 */
	public void setPageDisplayLength(Integer pageDisplayLength) {
		this.pageDisplayLength = pageDisplayLength;
	}

	/**
	 * @return the sortDir
	 */
	public String getSortDir() {
		return sortDir;
	}

	/**
	 * @param sortDir
	 *            the sortDir to set
	 */
	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	/**
	 * @return the sortingProperty
	 */
	public String getSortingProperty() {
		return sortingProperty;
	}

	/**
	 * @param sortingProperty
	 *            the sortingProperty to set
	 */
	public void setSortingProperty(String sortingProperty) {
		this.sortingProperty = sortingProperty;
	}

	/**
	 * @return the errorType
	 */
	public String getErrorType() {
		return errorType;
	}

	/**
	 * @param errorType
	 *            the errorType to set
	 */
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	/**
	 * @return the statusType
	 */
	public String getStatusType() {
		return statusType;
	}

	/**
	 * @param statusType
	 *            the statusType to set
	 */
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	/**
	 * @return the yearType
	 */
	public String getYearType() {
		return yearType;
	}

	/**
	 * @param yearType
	 *            the yearType to set
	 */
	public void setYearType(String yearType) {
		this.yearType = yearType;
	}

	/**
	 * @return the yearText
	 */
	public String getYearText() {
		return yearText;
	}

	/**
	 * @param yearText
	 *            the yearText to set
	 */
	public void setYearText(String yearText) {
		this.yearText = yearText;
	}

	/**
	 * @return the portfolioType
	 */
	public String getPortfolioType() {
		return portfolioType;
	}

	/**
	 * @param portfolioType
	 *            the portfolioType to set
	 */
	public void setPortfolioType(String portfolioType) {
		this.portfolioType = portfolioType;
	}

	/**
	 * @return the isAccountSelected
	 */
	public boolean isAccountSelected() {
		return isAccountSelected;
	}

	/**
	 * @param isAccountSelected
	 *            the isAccountSelected to set
	 */
	public void setAccountSelected(boolean isAccountSelected) {
		this.isAccountSelected = isAccountSelected;
	}

	/**
	 * @return the isAdmin
	 */
	public boolean isAdmin() {
		return isAdmin;
	}

	/**
	 * @param isAdmin the isAdmin to set
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	/**
	 * @return the searchParam
	 */
	public String getSearchParam() {
		return searchParam;
	}

	/**
	 * @param searchParam the searchParam to set
	 */
	public void setSearchParam(String searchParam) {
		this.searchParam = searchParam;
	}
	
	
}