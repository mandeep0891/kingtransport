package com.kingtransport.service;

import java.util.List;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.AccountDetailDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.AccountDetail;

/**
 * Create interface for account service
 * 
 * @author Mandeep Singh Gill
 *
 */
public interface AccountDetailService {

	public AccountDetail getAccoutnDetailById(Long accountId);

	public ResponseDto save(AccountDetailDto accountDetailDto, String userName);

	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public List<AccountDetailDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public ResponseDto selectAccount(String username, Long companyId);

	public ResponseDto selectAdditionalAccount(String username, Long accountId, Long companyId);
}
