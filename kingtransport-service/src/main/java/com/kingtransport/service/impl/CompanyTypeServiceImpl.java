package com.kingtransport.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.CompanyType;
import com.kingtransport.repositories.CompanyTypeRepository;
import com.kingtransport.service.CompanyTypeService;

@Service(value = "companyTypeService")
public class CompanyTypeServiceImpl extends BaseServiceImpl<CompanyType, Long> implements CompanyTypeService {

	private static final Logger logger = LoggerFactory.getLogger(CompanyTypeServiceImpl.class);

	private CompanyTypeRepository companyTypeRepository;

	/**
	 * @param companyRepository the companyRepository to set
	 */
	@Autowired
	public void setCompanyTypeRepository(CompanyTypeRepository companyTypeRepository) {
		this.companyTypeRepository = companyTypeRepository;
		setBaseRepository(companyTypeRepository);
		logger.info(
				"**** companyTypeRepository is autowired using setter injection******" + this.companyTypeRepository);
	}

	/*
	 * R
	 */
	@Override
	public CompanyType getCompanyType(Long id) {
		Optional<CompanyType> companyType = findById(id);
		if (companyType.isPresent()) {
			return companyType.get();
		}
		return null;
	}

	/*
	 * R
	 */
	@Override
	public List<SelectDto> getCompanyType() {
		return companyTypeRepository.selectCompanyType();
	}
}
