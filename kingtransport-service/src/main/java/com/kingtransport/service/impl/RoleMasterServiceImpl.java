package com.kingtransport.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.RoleMasterDto;
import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.RoleMaster;
import com.kingtransport.repositories.RoleMasterRepository;
import com.kingtransport.service.RoleMasterService;
import com.kingtransport.tools.IConstants;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
@Service(value = "roleMasterService")
public class RoleMasterServiceImpl extends BaseServiceImpl<RoleMaster, Long> implements RoleMasterService {

	private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private RoleMasterRepository roleMasterRepository;

	@Autowired
	public void setAccountRepository(RoleMasterRepository roleMasterRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.roleMasterRepository = roleMasterRepository;
		setBaseRepository(roleMasterRepository);
	}

	@Override
	public List<RoleMasterDto> findRoleDtoByAccountId(Long id) {
		return roleMasterRepository.findRoleDTOByAccountId(id);
	}

	@Override
	public RoleMaster findByName(String roleName) {
		return roleMasterRepository.findByName(roleName);
	}

	@Override
	public ResponseDto selectRole(String username, String role) {
		List<SelectDto> selectDtos = new ArrayList<SelectDto>();
		ResponseDto responseDTO = null;
		try {
			if (role.equals("employee")) {
				selectDtos = roleMasterRepository.selectRoleForDriver();
			} else {
				selectDtos = roleMasterRepository.selectRole();
			}
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), selectDtos,
					IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

}
