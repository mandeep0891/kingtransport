package com.kingtransport.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingtransport.dto.AccountDto;
import com.kingtransport.entities.Account;
import com.kingtransport.repositories.AccountRepository;
import com.kingtransport.service.AccountService;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
@Service(value = "accountService")
public class AccountServiceImpl extends BaseServiceImpl<Account, Long> implements AccountService {
	private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	public void setAccountRepository(AccountRepository accountRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.accountRepository = accountRepository;
		setBaseRepository(accountRepository);
	}

	@Override
	public AccountDto findByUserName(String username) {
		return accountRepository.findAccountDTOByUserName(username);
	}

	/*
	 * R
	 */
	@Override
	public Account getByUserName(String userName) {
		return accountRepository.findByUserName(userName);
	}

	@Override
	public Account saveAccount(Account account) {
		account = this.save(account);
		return account;
	}
}
