package com.kingtransport.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.TrailerDto;
import com.kingtransport.entities.Company;
import com.kingtransport.entities.Trailer;
import com.kingtransport.repositories.TrailerRepository;
import com.kingtransport.repositories.TrailerRepositoryCustom;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.TrailerService;
import com.kingtransport.tools.IConstants;

@Service(value = "trailerService")
public class TrailerServiceImpl extends BaseServiceImpl<Trailer, Long> implements TrailerService {

	private Logger logger = LoggerFactory.getLogger(TrailerServiceImpl.class);

	@Autowired
	private TrailerRepository trailerRepository;

	@Autowired
	private TrailerRepositoryCustom trailerRepositoryCustom;

	@Autowired
	private CompanyService companyService;

	@Autowired
	public void setTrailerRepository(TrailerRepository trailerRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.trailerRepository = trailerRepository;
		setBaseRepository(trailerRepository);
	}

	@Override
	public ResponseDto createTrailer(TrailerDto trailerDto) {
		Trailer trailer = new Trailer();
		ResponseDto responseDTO = null;
		try {
			if (trailerDto.getId() != null) {
				trailer.setId(trailerDto.getId());
			} else {
				if (trailerRepository.validateByTrailerNoAndName(trailerDto.getTrailerNo(), trailerDto.getName()) > 0) {
					responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(),
							2, "Trailer with same number already exist in database");
					return responseDTO;
				}
			}
			trailer.setTrailerNo(trailerDto.getTrailerNo());
			trailer.setName(trailerDto.getName());
			Optional<Company> company = companyService.findById(trailerDto.getCompanyId());
			if (company.isPresent()) {
				trailer.setCompany(company.get());
			}
			trailer = save(trailer);
			trailerDto.setId(trailer.getId());
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), trailerDto,
					"Record Updated Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return trailerRepositoryCustom.findCountByFilter(dataTableAttributes, username, role);
	}

	@Override
	public List<TrailerDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return trailerRepositoryCustom.findByFilter(dataTableAttributes, username, role);
	}

	@Override
	public ResponseDto selectTrailer(Long companyId) {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(),
					trailerRepository.selectTrailer(companyId), IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}
}
