package com.kingtransport.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.CargosDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.Account;
import com.kingtransport.entities.Cargos;
import com.kingtransport.entities.Company;
import com.kingtransport.entities.Trailer;
import com.kingtransport.entities.Truck;
import com.kingtransport.repositories.CargosRepository;
import com.kingtransport.repositories.CargosRepositoryCustom;
import com.kingtransport.service.AccountService;
import com.kingtransport.service.CargosService;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.TrailerService;
import com.kingtransport.service.TruckService;
import com.kingtransport.tools.IConstants;

@Service(value = "cargosService")
public class CargosServiceImpl extends BaseServiceImpl<Cargos, Long> implements CargosService {

	private Logger logger = LoggerFactory.getLogger(CargosServiceImpl.class);

	@Autowired
	private CargosRepository cargosRepository;

	@Autowired
	private AccountService accountService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private TruckService truckService;

	@Autowired
	private TrailerService trailerService;
	
	@Autowired
	private CargosRepositoryCustom cargosRepositoryCustom;

	@Autowired
	public void setCargosRepository(CargosRepository cargosRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.cargosRepository = cargosRepository;
		setBaseRepository(cargosRepository);
	}

	@Override
	public ResponseDto createCargos(CargosDto cargosDto) {
		Cargos cargos = new Cargos();
		ResponseDto responseDTO = null;
		try {
			if (cargosRepository.findCargoByCompanyIdAndDriverIdAndADriverIdAndTruckIdAndTrailerId(
					cargosDto.getCompanyId(), cargosDto.getDriverId(), cargosDto.getAdditionalDriverId(),
					cargosDto.getTruckId(), cargosDto.getTrailerId()) > 0) {
				return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
						"Record already exist for this combination.. ");
			} else {
				if (cargosDto.getId() != null) {
					
					cargos = cargosRepository.getOne(cargosDto.getId());
					if(cargos.getNoOfCargos() > 0) {
						return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
								"Can't update this record.. ");
					}
				} else {

					cargos.setNoOfCargos(0);
					cargos.setTotalBilableHours(0);
				}
				Optional<Account> accounts = accountService.findById(cargosDto.getDriverId());
				if (accounts.isPresent()) {
					cargos.setDriver(accounts.get());
				}
				Optional<Account> additionalAccounts = accountService.findById(cargosDto.getAdditionalDriverId());
				if (additionalAccounts.isPresent()) {
					cargos.setAdditionalDriver(additionalAccounts.get());
				}
				Optional<Company> companies = companyService.findById(cargosDto.getCompanyId());
				if (companies.isPresent()) {
					cargos.setCompany(companies.get());
				}
				Optional<Truck> trucks = truckService.findById(cargosDto.getTruckId());
				if (trucks.isPresent()) {
					cargos.setTruck(trucks.get());
				}
				Optional<Trailer> trailers = trailerService.findById(cargosDto.getTrailerId());
				if (trailers.isPresent()) {
					cargos.setTrailer(trailers.get());
				}
				cargosDto.setNoOfCargos(cargos.getNoOfCargos());
				cargosRepository.save(cargos);
				cargosDto.setId(cargos.getId());
				responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), cargosDto,
						IConstants.SUCCESS_MESSAGE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role,
			String searchType, String companyId) {
		return cargosRepositoryCustom.findCountByFilter(dataTableAttributes, username, role, searchType, companyId);
	}

	@Override
	public List<CargosDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role,
			String searchType, String companyId) {
		return cargosRepositoryCustom.findByFilter(dataTableAttributes, username, role, searchType, companyId);
	}

}
