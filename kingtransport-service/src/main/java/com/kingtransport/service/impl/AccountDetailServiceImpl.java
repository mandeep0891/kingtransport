package com.kingtransport.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.AccountDetailDto;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.Account;
import com.kingtransport.entities.AccountDetail;
import com.kingtransport.entities.AccountRole;
import com.kingtransport.entities.Address;
import com.kingtransport.entities.Company;
import com.kingtransport.entities.RoleMaster;
import com.kingtransport.repositories.AccountDetailRepository;
import com.kingtransport.repositories.AccountRepositoryCustom;
import com.kingtransport.service.AccountDetailService;
import com.kingtransport.service.AccountService;
import com.kingtransport.service.AddressService;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.RoleMasterService;
import com.kingtransport.tools.IConstants;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
@Service(value = "accountDetailService")
public class AccountDetailServiceImpl extends BaseServiceImpl<AccountDetail, Long> implements AccountDetailService {
	private Logger logger = LoggerFactory.getLogger(AccountDetailServiceImpl.class);

	@Autowired
	private AccountDetailRepository accountDetailRepository;
	@Autowired
	private AccountService accountService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private RoleMasterService roleMasterService;
	@Autowired
	private AccountRepositoryCustom accountRepositoryCustom;

	@Autowired
	public void setAccountDetailRepository(AccountDetailRepository accountDetailRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.accountDetailRepository = accountDetailRepository;
		setBaseRepository(accountDetailRepository);
	}

	@Override
	public AccountDetail getAccoutnDetailById(Long accountId) {
		Long accountDetailId = accountDetailRepository.findAccountDTOByUserName(accountId);
		Optional<AccountDetail> accDetails = findById(accountDetailId);
		if (accDetails.isPresent()) {
			return accDetails.get();
		}
		return null;
	}

	@Override
	public ResponseDto save(AccountDetailDto accountDetailDto, String userName) {
		Set<AccountDetail> accountDetails = new HashSet<AccountDetail>();
		AccountDetail accountDetail = new AccountDetail();
		ResponseDto responseDTO = null;
		Account logedinAccount = accountService.getByUserName(userName);
		try {
			// Account
			Account account = new Account();
			if (accountDetailDto.getId() != null && !accountDetailDto.getId().equals("")) {
				account.setId(accountDetailDto.getAccountId());
				accountDetail.setId(accountDetailDto.getId());

			}
			account.setPassword(encodePassword(createPassword(6, 1, 1, 1)));
			account.setUserName(accountDetailDto.getEmail().split("@")[0]);
			account.setStatus("Active");
			// Account Detail
			Date date = new Date();
			accountDetail.setCreatedDate(date);
			accountDetail.setIsArchived(true);
			accountDetail.setModifiedDate(date);
			accountDetail.setDob(accountDetailDto.getDob());
			accountDetail.setEmail(accountDetailDto.getEmail());
			accountDetail.setFirstName(accountDetailDto.getFirstName());
			accountDetail.setLastName(accountDetailDto.getLastName());
			accountDetail.setPhoneNumber(accountDetailDto.getPhoneNumber());
			accountDetail.setPhoneNumber1(accountDetailDto.getPhoneNumber());
			accountDetail.setProfileImage(accountDetailDto.getProfileImage());
			accountDetail.setSsNo(accountDetailDto.getSsNo());
			accountDetail.setCreatedBy(logedinAccount);
			accountDetail.setModifiedBy(logedinAccount);
			// Address
			Address address = new Address();
			if (accountDetailDto.getAddressId() != null && !accountDetailDto.getAddressId().equals("")) {
				address.setId(accountDetailDto.getAddressId());
			}
			address.setId(accountDetailDto.getAddressId());
			address.setAddress1(accountDetailDto.getAddress1());
			address.setAddress2(accountDetailDto.getAddress2());
			address.setAddress3(accountDetailDto.getAddress3());
			address.setCity(accountDetailDto.getCity());
			address.setCountry(accountDetailDto.getCountry());
			address.setPostalCode(accountDetailDto.getPostalCode());
			address.setState(accountDetailDto.getState());
			address = addressService.save(address);
			// Company
			Optional<Company> company = companyService.findById(accountDetailDto.getCompanyId());
			if (company.isPresent()) {
				accountDetail.setCompany(company.get());
			}

			accountDetail.setAddress(address);
			accountDetail.setAccount(account);
			accountDetails.add(accountDetail);

			// Acount Role
			Set<AccountRole> accountRoles = new HashSet<AccountRole>();
			AccountRole accountRole = new AccountRole();
			if (accountDetailDto.getAccountRoleId() != null && !accountDetailDto.getAccountRoleId().equals("")) {
				accountRole.setId(accountDetailDto.getAccountRoleId());
			}
			// Role Master
			Optional<RoleMaster> roleMaster = roleMasterService.findById(accountDetailDto.getRoleId());
			if (roleMaster.isPresent()) {
				accountRole.setRoleMaster(roleMaster.get());
			}
			accountRole.setAccount(account);
			account.setAccountRole(accountRoles);
			accountRoles.add(accountRole);
			account.setAccountDetails(accountDetails);
			account.setAccountRole(accountRoles);

			account = accountService.saveAccount(account);

			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), accountDetailDto,
					IConstants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	/**
	 * create random password for new user
	 * 
	 * @return password
	 */
	private String createPassword(int length, int noOfCAPSAlpha, int noOfDigits, int noOfSplChars) {
		StringBuilder result = new StringBuilder();
		final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
		final String NUM = "0123456789";
		final String SPL_CHARS = "!@#$%^&*_=+-/";

		while (length > 0) {
			Random rand = new Random();
			for (int i = 0; i < noOfCAPSAlpha; i++) {
				result.append(ALPHA_CAPS.charAt(rand.nextInt(ALPHA_CAPS.length())));
				length--;
			}
			for (int i = 0; i < noOfDigits; i++) {
				result.append(NUM.charAt(rand.nextInt(NUM.length())));
				length--;
			}
			for (int i = 0; i < noOfSplChars; i++) {
				result.append(SPL_CHARS.charAt(rand.nextInt(SPL_CHARS.length())));
				length--;
			}
			for (int i = 0; i < 3; i++) {
				result.append(ALPHA.charAt(rand.nextInt(ALPHA.length())));
				length--;
			}
		}
		return result.toString();

	}

	/**
	 * Encode password.
	 *
	 * @param newPassword the new password
	 * @return the string
	 */
	public String encodePassword(String newPassword) {
		String password = new BCryptPasswordEncoder().encode(newPassword);
		return password;
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		CompanyDto companyDto = companyService.getCompanyByUserName(username);
		return accountRepositoryCustom.findCountByFilter(dataTableAttributes, username, role, companyDto.getId());
	}

	@Override
	public List<AccountDetailDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		CompanyDto companyDto = companyService.getCompanyByUserName(username);
		return accountRepositoryCustom.findByFilter(dataTableAttributes, username, role, companyDto.getId());
	}

	@Override
	public ResponseDto selectAccount(String username, Long companyId) {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(),
					accountDetailRepository.selectAccount(username, companyId), IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public ResponseDto selectAdditionalAccount(String username, Long accountId, Long companyId) {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(),
					accountDetailRepository.selectAdditionalAccount(username, accountId, companyId),
					IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

}
