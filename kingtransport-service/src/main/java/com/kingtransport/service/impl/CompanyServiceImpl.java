package com.kingtransport.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.AddressDto;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.Account;
import com.kingtransport.entities.Address;
import com.kingtransport.entities.Company;
import com.kingtransport.repositories.CompanyRepository;
import com.kingtransport.repositories.CompanyRepositoryCustom;
import com.kingtransport.service.AccountService;
import com.kingtransport.service.AddressService;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.CompanyTypeService;
import com.kingtransport.tools.IConstants;

@Service(value = "companyService")
public class CompanyServiceImpl extends BaseServiceImpl<Company, Long> implements CompanyService {

	private static final Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);

	private CompanyRepository companyRepository;

	@Autowired
	private CompanyRepositoryCustom companyRepositoryCustom;

	@Autowired
	private CompanyTypeService companyTypeService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private AddressService addressService;

	/**
	 * @param companyRepository the companyRepository to set
	 */
	@Autowired
	public void setCompanyRepository(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
		setBaseRepository(companyRepository);
		logger.info("**** companyRepository is autowired using setter injection******" + this.companyRepository);
	}

	@Override
	public ResponseDto getCompanyType() {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), companyTypeService.getCompanyType(),
					IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public ResponseDto save(CompanyDto companyDto, String userName) {
		ResponseDto responseDTO = null;
		Address address = null;
		Company company = new Company();
		try {
			if (companyDto.getId() != null) {
				company.setId(companyDto.getId());
			}
			Date date = new Date();
			company.setCreatedDate(date);
			company.setIsArchived(false);
			company.setModifiedDate(date);
			company.setContactNumber(companyDto.getContactNumber());
			company.setDotNo(companyDto.getDotNo());
			company.setEmail(companyDto.getEmail());
			company.setMcNo(companyDto.getMcNo());
			company.setName(companyDto.getName());
			company.setStatus(IConstants.ACTIVE);
			company.setTaxId(companyDto.getTaxId());
			AddressDto addressDto = new AddressDto();
			addressDto.setId(companyDto.getAddressId());
			addressDto.setAddress1(companyDto.getAddress1());
			addressDto.setAddress2(companyDto.getAddress2());
			addressDto.setAddress3(companyDto.getAddress3());
			addressDto.setCity(companyDto.getCity());
			addressDto.setCountry(companyDto.getCountry());
			addressDto.setPostalCode(companyDto.getPostalCode());
			addressDto.setState(companyDto.getState());
			address = addressService.saveAddress(addressDto);
			company.setAddress(address);
			company.setCompanyType(companyTypeService.getCompanyType(companyDto.getCompanyTypeId()));
			Account account = accountService.getByUserName(userName);
			company.setCreatedBy(account);
			company.setModifiedBy(account);
			companyRepository.save(company);
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), companyDto, IConstants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public CompanyDto getCompanyByUserName(String username) {
		return companyRepository.findACompanyDtoByUserName(username);
	}

	/*
	 * R
	 */
	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return companyRepositoryCustom.findCountByFilter(dataTableAttributes, username, role);
	}

	/*
	 * R
	 */
	@Override
	public List<CompanyDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return companyRepositoryCustom.findByFilter(dataTableAttributes, username, role);
	}

	/*
	 * R
	 */
	@Override
	public ResponseDto selectCompany(String username, String role) {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(),
					companyRepository.selectCompanyByUsername(username), IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public ResponseDto selectCompany(String username, String role, String selectedRole) {
		List<SelectDto> selectDtos = new ArrayList<SelectDto>();
		ResponseDto responseDTO = null;
		try {
			if (selectedRole.equals("employee")) {
				selectDtos = companyRepository.selectCompany();
			} else {
				selectDtos = companyRepository.selectCompanyByUsername(username);
			}
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), selectDtos,
					IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

}
