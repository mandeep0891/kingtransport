package com.kingtransport.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingtransport.entities.AccountRole;
import com.kingtransport.repositories.AccountRoleRepository;
import com.kingtransport.service.AccountRoleService;

@Service(value = "accountRoleService")
public class AccountRoleServiceImpl extends BaseServiceImpl<AccountRole, Long> implements AccountRoleService {
	private Logger logger = LoggerFactory.getLogger(AccountDetailServiceImpl.class);

	@Autowired
	private AccountRoleRepository accountRoleRepository;

	@Autowired
	public void setAccountRoleRepository(AccountRoleRepository accountRoleRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.accountRoleRepository = accountRoleRepository;
		setBaseRepository(accountRoleRepository);
	}

	public AccountRole save(AccountRole accountRole) {
		return accountRoleRepository.save(accountRole);
	}

}
