package com.kingtransport.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingtransport.dto.AddressDto;
import com.kingtransport.entities.Address;
import com.kingtransport.repositories.AddressRepository;
import com.kingtransport.service.AddressService;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
@Service(value = "addressService")
public class AddressServiceImpl extends BaseServiceImpl<Address, Long> implements AddressService {
	private Logger logger = LoggerFactory.getLogger(AddressServiceImpl.class);

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	public void setAddressRepository(AddressRepository addressRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.addressRepository = addressRepository;
		setBaseRepository(addressRepository);
	}

	/*
	 * R
	 */
	@Override
	public Address saveAddress(AddressDto addressDto) {
		Address address = new Address();
		if (addressDto.getId() != null) {
			address.setId(addressDto.getId());
		}
		address.setAddress1(addressDto.getAddress1());
		address.setAddress2(addressDto.getAddress2());
		address.setAddress3(addressDto.getAddress3());
		address.setCity(addressDto.getCity());
		address.setCountry(addressDto.getCountry());
		address.setPostalCode(addressDto.getPostalCode());
		address.setState(addressDto.getState());
		address = save(address);
		return address;
	}

	@Override
	public Address getAddressById(Long id) {
		Optional<Address> address = addressRepository.findById(id);
		if (address.isPresent()) {
			return address.get();
		}
		return null;
	}

}
