/**
 * 
 */
package com.kingtransport.service.impl;

import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.EmailTemplate;
import com.kingtransport.repositories.EmailRepository;
import com.kingtransport.service.EmailService;
import com.kingtransport.tools.IConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mandeep Singh Gill
 *
 */
@Service("emailService")
public class EmailServiceImpl extends BaseServiceImpl<EmailTemplate, Long> implements EmailService {

	private Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${spring.mail.username}")
	private String from;

	private EmailRepository emailRepository;

	/**
	 * @param emailRepository the emailRepository to set
	 */
	@Autowired
	public void setEmailRepository(EmailRepository emailRepository) {
		this.emailRepository = emailRepository;
		setBaseRepository(emailRepository);
		logger.info("**** emailTemplateRepository is autowired using setter injection******"
				+ this.emailRepository);
	}

	@Override
	public ResponseDto sendMail(String[] to, HashMap<String, String> params) {
		logger.info("*************************send email service start**************************************");
		ResponseDto responseDto = new ResponseDto();
		try {
			EmailTemplate emailTemplate = emailRepository.findTemplateById(IConstants.EmailTempate.EMAIL_TO_DISPATCH);
			String emailBody = emailTemplate.getContent();
			// setting dynamic parameters in email content
			for (Map.Entry<String, String> param : params.entrySet()) {
				String key = param.getKey();
				String value = param.getValue();
				emailBody = emailBody.replaceAll(":" + key, value);
			}
			logger.info(emailBody);
			MimeMessage mail = javaMailSender.createMimeMessage();
			MimeMessageHelper helper;
			helper = new MimeMessageHelper(mail, true);
			helper.setTo(to);
			helper.setFrom(from);
			helper.setSubject(params.get("subject"));
			helper.setText(emailBody, true);
			javaMailSender.send(mail);
			logger.info("*************************send email service ends**************************************");
		} catch (Exception e){
			e.printStackTrace();
		}
		return responseDto;
	}
}
