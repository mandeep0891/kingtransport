package com.kingtransport.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.TruckDto;
import com.kingtransport.entities.Company;
import com.kingtransport.entities.Truck;
import com.kingtransport.repositories.TruckRepository;
import com.kingtransport.repositories.TruckRepositoryCustom;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.TruckService;
import com.kingtransport.tools.IConstants;

@Service(value = "truckService")
public class TruckServiceImpl extends BaseServiceImpl<Truck, Long> implements TruckService {

	private Logger logger = LoggerFactory.getLogger(TruckServiceImpl.class);

	@Autowired
	private TruckRepository truckRepository;

	@Autowired
	private TruckRepositoryCustom truckRepositoryCustom;

	@Autowired
	private CompanyService companyService;

	@Autowired
	public void setTruckRepository(TruckRepository truckRepository) {
		logger.info("****userRepo is initialed using setter injection****");
		this.truckRepository = truckRepository;
		setBaseRepository(truckRepository);
	}

	@Override
	public ResponseDto createTruck(TruckDto truckDto) {
		Truck truck = new Truck();
		ResponseDto responseDTO = null;
		try {
			if (truckDto.getId() != null) {
				truck.setId(truckDto.getId());
			} else {
				if (truckRepository.validateByTruckNoAndName(truckDto.getTruckNo(), truckDto.getName()) > 0) {
					responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(),
							2, "Truck with same number already exist in database");
					return responseDTO;
				}
			}
			truck.setTruckNo(truckDto.getTruckNo());
			truck.setName(truckDto.getName());
			Optional<Company> company = companyService.findById(truckDto.getCompanyId());
			if (company.isPresent()) {
				truck.setCompany(company.get());
			}
			truck = save(truck);
			truckDto.setId(truck.getId());
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), truckDto,
					"Record Updated Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return truckRepositoryCustom.findCountByFilter(dataTableAttributes, username, role);
	}

	@Override
	public List<TruckDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role) {
		return truckRepositoryCustom.findByFilter(dataTableAttributes, username, role);
	}

	@Override
	public ResponseDto selectTruck(Long companyId) {
		ResponseDto responseDTO = null;
		try {
			responseDTO = new ResponseDto(HttpStatus.OK, HttpStatus.OK.value(), truckRepository.selectTruck(companyId),
					IConstants.FETCHED_SUCCESSFULLY);
		} catch (Exception e) {
			responseDTO = new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					e.getMessage());
		}
		return responseDTO;
	}

}
