package com.kingtransport.service;

import java.util.Optional;

import com.kingtransport.dto.AccountDto;
import com.kingtransport.entities.Account;

/**
 * Create interface for account service
 * 
 * @author Mandeep Singh Gill
 *
 */
public interface AccountService {

	public AccountDto findByUserName(String username);

	public Account getByUserName(String userName);

	public Account saveAccount(Account account);

	public Optional<Account> findById(Long id);

}
