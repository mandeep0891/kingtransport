package com.kingtransport.service;

import java.util.List;
import java.util.Optional;

import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.RoleMasterDto;
import com.kingtransport.entities.RoleMaster;

public interface RoleMasterService {

	public List<RoleMasterDto> findRoleDtoByAccountId(Long id);

	public RoleMaster findByName(String roleName);

	public Optional<RoleMaster> findById(Long id);
	
	public ResponseDto selectRole(String username, String role);

}
