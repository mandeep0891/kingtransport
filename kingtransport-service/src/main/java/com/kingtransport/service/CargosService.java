package com.kingtransport.service;

import java.util.List;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.CargosDto;
import com.kingtransport.dto.ResponseDto;

public interface CargosService {

	public ResponseDto createCargos(CargosDto cargosDto);
	
	/*
	 * Paging in Company
	 */
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role, String searchType, String companyId);

	/*
	 * Paging in Company
	 */
	public List<CargosDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role, String searchType, String companyId);


}
