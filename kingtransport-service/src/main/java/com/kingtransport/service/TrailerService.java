package com.kingtransport.service;

import java.util.List;
import java.util.Optional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.TrailerDto;
import com.kingtransport.entities.Trailer;

public interface TrailerService {
	public ResponseDto createTrailer(TrailerDto trailerDto);

	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public List<TrailerDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public ResponseDto selectTrailer(Long companyId);

	public Optional<Trailer> findById(Long id);
}
