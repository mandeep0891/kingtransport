package com.kingtransport.service;

import com.kingtransport.dto.AddressDto;
import com.kingtransport.entities.Address;

/**
 * Create interface for account service
 * 
 * @author Mandeep Singh Gill
 *
 */
public interface AddressService {

	public Address saveAddress(AddressDto addressDto);

	public Address getAddressById(Long id);

	public Address save(Address address);
}
