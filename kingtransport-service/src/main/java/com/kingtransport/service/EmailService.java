package com.kingtransport.service;

import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.EmailTemplate;

import java.util.HashMap;

/**
 * @author Mandeep Singh Gill
 *
 */
public interface EmailService extends BaseService<EmailTemplate, Long> {
	/**
	 * send mail to new created user
	 * 
	 * @param to
	 * @param params
	 */
	public ResponseDto sendMail(String[] to, HashMap<String, String> params);
	 
}
