package com.kingtransport.service;

import java.util.List;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.CompanyType;

public interface CompanyTypeService {
	/*
	 * get company Type by id during company update
	 */
	public CompanyType getCompanyType(Long id);

	/*
	 * fetch company type dropdown to save new company
	 */
	public List<SelectDto> getCompanyType();
}
