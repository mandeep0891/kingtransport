package com.kingtransport.service;

import com.kingtransport.entities.AccountRole;

public interface AccountRoleService {

	public AccountRole save(AccountRole accountRole);

}
