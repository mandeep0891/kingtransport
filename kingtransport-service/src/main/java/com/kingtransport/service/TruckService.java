package com.kingtransport.service;

import java.util.List;
import java.util.Optional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.TruckDto;
import com.kingtransport.entities.Truck;

public interface TruckService {

	public ResponseDto createTruck(TruckDto truckDao);

	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public List<TruckDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	public ResponseDto selectTruck(Long companyId);

	public Optional<Truck> findById(Long id);
}
