package com.kingtransport.service;

import java.util.List;
import java.util.Optional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.entities.Company;

public interface CompanyService extends BaseService<Company, Long> {

	/*
	 * getCompanyType in Company
	 */
	public ResponseDto getCompanyType();

	/*
	 * for create and update company  in Company
	 */
	public ResponseDto save(CompanyDto companyDto, String userName);

	/*
	 * Paging in Company
	 */
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	/*
	 * Paging in Company
	 */
	public List<CompanyDto> findByFilter(DataTableAttributes dataTableAttributes, String username, String role);

	
	public CompanyDto getCompanyByUserName(String username);

	public Optional<Company> findById(Long id);

	public ResponseDto selectCompany(String username, String role);
	
	public ResponseDto selectCompany(String username, String role, String selectedRole);
	
}
