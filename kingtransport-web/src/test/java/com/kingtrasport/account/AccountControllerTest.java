package com.kingtrasport.account;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.company.CompanyControllerTest;
import com.kingtransport.dto.AccountDetailDto;
public class AccountControllerTest extends KingTransportApplicationTest {

	private static Logger logger = LoggerFactory.getLogger(CompanyControllerTest.class);

	private static String URI = "/api/account";
	String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbXJpay1raW5ndHJhbnNwb3J0Iiwic2NvcGVzIjpbImZvdW5kZXIiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiaWF0IjoxNjM5OTM2NzQzLCJleHAiOjE2Mzk5Mzg1NDN9.gOKBp2PakVaLrG1vE8IsovLqKypohf5Ja3SO23tXKposR0qzHEyz1JV4eiRk8QMUTRMRITTmlhws-i2LyFRfKg";


	//@Test
	public void selectRoleDrop() throws Exception {
		logger.info("Inside select role");
		URI = URI + "/role";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}

	//@Test
	public void selectCompanyDrop() throws Exception {
		logger.info("Inside select company");
		URI = URI + "/company?selectedRole=employee";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}

	//@Test
	public void create() throws Exception {
		logger.info("Inside create account Test.");
		URI = URI + "/create";
		Date date = new Date();
		AccountDetailDto accountDetailDto = new AccountDetailDto(1L, 3L, 847908956L, "Mandeep", "Gill",
				"mandeep.gill.java@mgial.com", "9914924919", "", 1L, "VPO Charik", "Distt Moga", "Gholia Road Charik",
				"Moga", "Punjab", "India", "142001", date);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(accountDetailDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void update() throws Exception {
		logger.info("Inside update account Test.");
		URI = URI + "/update";
		Date date = new Date();
		try {

			AccountDetailDto accountDetailDto = new AccountDetailDto(4L, 4L, 12L, 2L, 4L, 4L, 847908956L, "Mandeep1",
					"Gill1", "mandeep.gill.java1@mgial.com", "9914924911", "", 1L, "VPO Charik1", "Distt Moga1",
					"Gholia Road Charik1", "Moga1", "Punjab1", "India1", "142002", date);
			MvcResult mvcResult = mockMvc
					.perform(MockMvcRequestBuilders.put(URI).contentType(MediaType.APPLICATION_JSON)
							.header("X-Authorization", "Bearer-" + token).content(mapToJson(accountDetailDto)))
					.andExpect(status().isOk()).andDo(print()).andReturn();
			int status = mvcResult.getResponse().getStatus();
			assertEquals(200, status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void list() throws Exception {
		URI = URI + "/paging?length=5&start=0&searchParam=";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

}
