package com.kingtransport.email;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.dto.EmailDto;
import com.kingtransport.service.EmailService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

public class EmailControllerTest extends KingTransportApplicationTest {
    private static Logger logger = LoggerFactory.getLogger(EmailControllerTest.class);
    private static String URI = null;

    @Autowired
    EmailService emailService;

    @Test
    public void emailSendTest() throws Exception {
        logger.info("***************************************Send Email Test Start**************************************************");
        URI = "/api/email/send";
        EmailDto emailDto = new EmailDto();
        String[] emails = {"mandeep.gill.java@gmail.com", "buttararash5@gmail.com"};
        emailDto.setEmails(emails);
        emailDto.setBody("We are free to dispatch the load. New one");
        emailDto.setName("Pandher, Amrik S");
        emailDto.setSubject("DispatchLoad");
        emailDto.setPhoneNumber("+1 425-900-7987");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", emailDto.getName());
        map.put("subject", emailDto.getSubject());
        map.put("phoneNumber", emailDto.getPhoneNumber());
        map.put("body", emailDto.getBody());
        emailService.sendMail(emails, map);
        logger.info("***************************************Login Test Start**************************************************");



    }
}

