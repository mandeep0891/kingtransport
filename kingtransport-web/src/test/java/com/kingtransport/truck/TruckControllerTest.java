package com.kingtransport.truck;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.dto.TruckDto;

public class TruckControllerTest extends KingTransportApplicationTest {

	private static String URI = "/api/trucks";

	String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbXJpay1raW5ndHJhbnNwb3J0Iiwic2NvcGVzIjpbImZvdW5kZXIiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiaWF0IjoxNjM4MzMzNzU5LCJleHAiOjE2MzgzMzU1NTl9.wDjejmf0-oCYjynx4pIgxxUn-SGhglVbrNqmFX7Q5S-p41I2_vJQ6PFRZ8J_9-5C9rbMG4lH0mDenW6RBWnDKg";

	//@Test
	public void selectDtos() throws Exception {
		URI = URI + "/company";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void create() throws Exception {
		URI = URI + "/create";
		TruckDto truckDto = new TruckDto(null, "RAM", "rm", 2L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(truckDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void update() throws Exception {
		URI = URI + "/update";
		TruckDto truckDto = new TruckDto(2L, "RAM1", "ram1", 2L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.put(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(truckDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void list() throws Exception {
		URI = URI + "/paging?length=5&start=0&searchParam=";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

}
