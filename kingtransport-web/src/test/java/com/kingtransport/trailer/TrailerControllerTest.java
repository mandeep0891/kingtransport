package com.kingtransport.trailer;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.dto.TrailerDto;

public class TrailerControllerTest extends KingTransportApplicationTest {
	private static Logger logger = LoggerFactory.getLogger(TrailerControllerTest.class);

	private static String URI = "/api/trailers";
	
	String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbXJpay1raW5ndHJhbnNwb3J0Iiwic2NvcGVzIjpbImZvdW5kZXIiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiaWF0IjoxNjM4MzMzNzU5LCJleHAiOjE2MzgzMzU1NTl9.wDjejmf0-oCYjynx4pIgxxUn-SGhglVbrNqmFX7Q5S-p41I2_vJQ6PFRZ8J_9-5C9rbMG4lH0mDenW6RBWnDKg";

	//@Test
	public void selectDtos() throws Exception {
		URI = URI + "/company";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
	
	
	//@Test
	public void create() throws Exception {
		URI = URI + "/create";
		TrailerDto trailerDto = new TrailerDto(null, "HONDA", "crv", 2L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(trailerDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
	
	//@Test
	public void update() throws Exception {
		URI = URI + "/update";
		TrailerDto trailerDto = new TrailerDto(2L, "HONDA1", "crv1", 2L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.put(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(trailerDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
	
	@Test
	public void list() throws Exception {
		URI = URI + "/paging?length=5&start=0&searchParam=";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
