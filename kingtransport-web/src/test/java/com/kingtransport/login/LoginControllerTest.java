package com.kingtransport.login;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;

public class LoginControllerTest extends KingTransportApplicationTest {
	private static Logger logger = LoggerFactory.getLogger(LoginControllerTest.class);
	private static String URI = null;

	@Test
	public void loginTest() throws Exception {
		logger.info(
				"***************************************Login Test Start**************************************************");
		URI = "/api/auth/login";
		String jsonString = "{\"username\":\"amrik-kingtransport\",\"password\":\"1\"}";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON).content(jsonString))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		JacksonJsonParser jsonParser = new JacksonJsonParser();
		String tokenData = jsonParser.parseMap(mvcResult.getResponse().getContentAsString()).get("data").toString();
		logger.info(tokenData);
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		logger.info(
				"***************************************Login Test Start**************************************************");

	}
}
