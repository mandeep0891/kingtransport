package com.kingtransport.cargo;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.dto.CargosDto;

public class CargosControllerTest extends KingTransportApplicationTest {
	private static Logger logger = LoggerFactory.getLogger(CargosControllerTest.class);

	private static String URI = "/api/cargos";

	String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbXJpay1raW5ndHJhbnNwb3J0Iiwic2NvcGVzIjpbImZvdW5kZXIiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiaWF0IjoxNjQxMDgyNTI3LCJleHAiOjE2NDEwODQzMjd9.1hLGtu2yeLAqyVb983o7pd1aRbs15YWHixW_-sOJMA20nXBbweTZpr5abI_YpdlQaDg9SRYhy6lfcsctv9TNKg";

	// @Test
	public void selectCompany() throws Exception {
		URI = URI + "/company";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	// @Test
	public void selectDriver() throws Exception {
		URI = URI + "/driver?companyId=2";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	// @Test
	public void selectAddionalDriver() throws Exception {
		URI = URI + "/additional/driver?companyId=2&driverId=4";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	// @Test
	public void selectTruck() throws Exception {
		URI = URI + "/truck?companyId=2";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void selectTrailer() throws Exception {
		URI = URI + "/trailer?companyId=2";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void create() throws Exception {
		URI = URI + "/create";
		CargosDto cargosDto = new CargosDto();
		cargosDto.setCompanyId(1L);
		cargosDto.setDriverId(3L);
		cargosDto.setAdditionalDriverId(4L);
		cargosDto.setTruckId(1L);
		cargosDto.setTrailerId(2L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(cargosDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
	
	//@Test
	public void update() throws Exception {
		URI = URI + "/update";
		CargosDto cargosDto = new CargosDto();
		cargosDto.setId(2L);
		cargosDto.setCompanyId(2L);
		cargosDto.setDriverId(3L);
		cargosDto.setAdditionalDriverId(4L);
		cargosDto.setTruckId(2L);
		cargosDto.setTrailerId(1L);
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(cargosDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
	
	@Test
	public void list() throws Exception {
		URI = URI + "/paging?companyId=1&length=5&start=0&searchType=Trailer&searchParam=HONDA";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
