package com.kingtransport.company;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.kingtransport.KingTransportApplicationTest;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.service.CompanyService;

public class CompanyControllerTest extends KingTransportApplicationTest {
	private static Logger logger = LoggerFactory.getLogger(CompanyControllerTest.class);

	private static String URI = "/api/company";

	@Autowired
	CompanyService companyService;

	String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbXJpay1raW5ndHJhbnNwb3J0Iiwic2NvcGVzIjpbImZvdW5kZXIiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiaWF0IjoxNjM5ODkzOTQ5LCJleHAiOjE2Mzk4OTU3NDl9.-5soUwBLQGYv8DfiC6R6aOTxKo2nNfnpglOen_hV0wbKUp2K9x-whHcuhrVe4zu1ZX24HaB6cCIoxvI6xnky_g";

	@Test
	public void getCompanyType() throws Exception {
		URI = URI + "/companyType";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	// @Test
	public void create() throws Exception {
		URI = URI + "/create";
		CompanyDto companyDto = new CompanyDto(null, 1L, 2L, "Transport28", "Kings28", "tax238", "mcNo28", "dorNo28",
				"mandeep.gill.java1@gmail.com", "2828282828", "Address28", "Address28", "Address28", "city28",
				"country28", "postalcode28", "state28");
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(companyDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	// @Test
	public void update() throws Exception {
		URI = URI + "/update";
		CompanyDto companyDto = new CompanyDto(1L, 1L, 2L, "Transport11", "Kings11", "tax123411", "mcNo123411",
				"dorNo123411", "mandeep.gill.java1@gmail.com", "2533351111", "Address111", "Address2311", "Address11",
				"city11", "country11", "postalcode11", "state11");
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.put(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token).content(mapToJson(companyDto)))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	//@Test
	public void list() throws Exception {
		URI = URI + "/paging?length=5&start=0&searchParam=King";
		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON)
						.header("X-Authorization", "Bearer-" + token))
				.andExpect(status().isOk()).andDo(print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
