package com.kingtransport.config;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kingtransport.auth.error.handler.RestAuthenticationEntryPoint;
import com.kingtransport.security.auth.ajax.AjaxAuthenticationProvider;
import com.kingtransport.security.auth.ajax.filter.AjaxLoginProcessingFilter;
import com.kingtransport.security.auth.ajax.handler.AjaxAwareAuthenticationFailureHandler;
import com.kingtransport.security.auth.ajax.handler.AjaxAwareAuthenticationSuccessHandler;
import com.kingtransport.security.auth.jwt.JwtAuthenticationProvider;
import com.kingtransport.security.auth.jwt.SkipPathRequestMatcher;
import com.kingtransport.security.auth.jwt.extractor.TokenExtractor;
import com.kingtransport.security.auth.jwt.filter.JwtTokenAuthenticationProcessingFilter;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         WebSecurityConfigurerAdapter Provides a convenient base class for
 *         creating a WebSecurityConfigurer instance. The implementation allows
 *         customization by overriding methods.
 *
 */

@EnableWebSecurity
public class WebSecurityConfigure extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(WebSecurityConfigure.class);
	/* properties added for jwt */
	public static final String API_BASED_AUTH_ENTRY_POINT = "/api/**";
	public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
	public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/api/auth/login";
	public static final String TOKEN_REFRESH_ENTRY_POINT = "/api/auth/token";
	private static final String[] BY_PASS_SECURITY_PERMITALL_URLS = { "/", "/contactUs/**", "/resources/**",
			FORM_BASED_LOGIN_ENTRY_POINT };

	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private AjaxAwareAuthenticationSuccessHandler ajaxAwareAuthenticationSuccessHandler;

	@Autowired
	private AjaxAwareAuthenticationFailureHandler ajaxAwareAuthenticationFailureHandler;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private AjaxAuthenticationProvider ajaxAuthenticationProvider;

	@Autowired
	private JwtAuthenticationProvider jwtAuthenticationProvider;

	@Autowired
	private TokenExtractor tokenExtractor;

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		logger.info("PasswordEncoder bean successfully initiated ");
		return new BCryptPasswordEncoder();
	}

	protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter() throws Exception {
		AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT,
				ajaxAwareAuthenticationSuccessHandler, ajaxAwareAuthenticationFailureHandler, objectMapper);
		filter.setAuthenticationManager(this.authenticationManager);
		return filter;
	}

	protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
		List<String> pathsToSkip = Arrays.asList(FORM_BASED_LOGIN_ENTRY_POINT, TOKEN_REFRESH_ENTRY_POINT);
		SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, API_BASED_AUTH_ENTRY_POINT);
		JwtTokenAuthenticationProcessingFilter filter = new JwtTokenAuthenticationProcessingFilter(
				ajaxAwareAuthenticationFailureHandler, tokenExtractor, matcher);
		filter.setAuthenticationManager(this.authenticationManager);
		return filter;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests().antMatchers(BY_PASS_SECURITY_PERMITALL_URLS).permitAll()
				.antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll().and().exceptionHandling()
				.authenticationEntryPoint(this.restAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers(API_BASED_AUTH_ENTRY_POINT).authenticated().and()
				.addFilterBefore(buildAjaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(),
						UsernamePasswordAuthenticationFilter.class);
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(ajaxAuthenticationProvider);
		auth.authenticationProvider(jwtAuthenticationProvider);
	}

}
