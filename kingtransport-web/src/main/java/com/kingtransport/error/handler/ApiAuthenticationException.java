package com.kingtransport.error.handler;

/**
 * 
 * @author Mandeep Singh Gill
 *
 *         Class to create custom exception thrown through our application
 */
public class ApiAuthenticationException extends RuntimeException {

	private static final long serialVersionUID = 12213141L;

	public ApiAuthenticationException(String message) {
		super(message);
	}

	public ApiAuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}
}