package com.kingtransport.error.handler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kingtransport.tools.ApiException;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Used to handle custom exceptions even handle existing exceptions
 *
 */
@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = { ApiRequestException.class })
	public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {

//Create payload containing exception details
		HttpStatus badrequest = HttpStatus.BAD_REQUEST;
		ApiException apiException = new ApiException(e.getMessage(), /* e, */ HttpStatus.BAD_REQUEST,
				ZonedDateTime.now(ZoneId.of("Z")));
//Return response entity
		ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(apiException, badrequest);
		return responseEntity;
	}

	@ExceptionHandler(value = { ApiAuthenticationException.class })
	public ResponseEntity<Object> handleApiRequestException(ApiAuthenticationException e) {

//Create payload containing exception details
		HttpStatus nonAuthoritativeInfo = HttpStatus.NON_AUTHORITATIVE_INFORMATION;
		ApiException apiException = new ApiException(e.getMessage(), /* e, */ HttpStatus.NON_AUTHORITATIVE_INFORMATION,
				ZonedDateTime.now(ZoneId.of("Z")));
//Return response entity
		ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(apiException, nonAuthoritativeInfo);
		return responseEntity;
	}
}
