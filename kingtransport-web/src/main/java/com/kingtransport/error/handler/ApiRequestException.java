package com.kingtransport.error.handler;

/**
 * 
 * @author Mandeep Singh Gill
 *
 *         Class to create custom exception thrown through our application
 */
public class ApiRequestException extends RuntimeException {

	private static final long serialVersionUID = 1213141L;

	public ApiRequestException(String message) {
		super(message);
	}

	public ApiRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
