package com.kingtransport;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kingtransport" })
public class KingTransportApplication extends SpringBootServletInitializer {
	private static final Logger logger = LoggerFactory.getLogger(KingTransportApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		logger.error(">>>>>>>>>>>>>>>>>>>>>>>>>:error");
		logger.warn(">>>>>>>>>>>>>>>>>>>>>>>>>>:warn");
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>:info");
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>:debug");
		return application.sources(KingTransportApplication.class);
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(KingTransportApplication.class, args);
		String[] beansArray = applicationContext.getBeanDefinitionNames();
		Arrays.sort(beansArray);
		for (String beanName : beansArray) {
			System.out.println(beanName);
		}
	}
}
