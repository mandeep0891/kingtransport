package com.kingtransport.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.datatables.DataTableJSON;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.CompanyService;
import com.kingtransport.tools.IConstants;

@RestController
@RequestMapping(value = "/api/company")
public class CompanyController {

	private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	private CompanyService companyService;

	@RequestMapping(value = "/companyType", method = { RequestMethod.GET })
	public ResponseDto getCompanyType(Authentication authentication, HttpServletRequest request) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		if (!role.equals(IConstants.Role.ADMIN_ROLE.getValue())
				&& !role.equals(IConstants.Role.FOUNDER_ROLE.getValue())) {
			return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					IConstants.ACCESS_DENIED);
		} else {
			return companyService.getCompanyType();
		}
	}

	/*
	 * create new company only by Admin/Founder
	 */
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	public ResponseDto create(@RequestBody final CompanyDto companyDto, Authentication authentication) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		if (!role.equals(IConstants.Role.ADMIN_ROLE.getValue())
				&& !role.equals(IConstants.Role.FOUNDER_ROLE.getValue())) {
			return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					IConstants.ACCESS_DENIED);
		} else {
			return companyService.save(companyDto, userContext.getUsername());
		}
	}

	@RequestMapping(value = "/update", method = { RequestMethod.PUT })
	public ResponseDto update(@RequestBody final CompanyDto companyDto, Authentication authentication) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		if (!role.equals(IConstants.Role.ADMIN_ROLE.getValue())
				&& !role.equals(IConstants.Role.FOUNDER_ROLE.getValue())) {
			return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					IConstants.ACCESS_DENIED);
		} else {
			return companyService.save(companyDto, userContext.getUsername());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/paging", method = { RequestMethod.GET }, produces = "application/json")
	public DataTableJSON list(Authentication authentication, HttpServletRequest request) {
		logger.info("********************************start company controller*****************************");
		String length = request.getParameter("length");
		String pageStart = request.getParameter("start");
		String value = request.getParameter("searchParam");
		DataTableJSON dataTableJSON = new DataTableJSON();
		DataTableAttributes dataTableAttributes = new DataTableAttributes(Integer.valueOf(pageStart),
				Integer.valueOf(length), null, null, value);
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		System.out.println(role);
		Long count = companyService.findCountByFilter(dataTableAttributes, userContext.getUsername(), role);
		logger.info("********************************count " + count + "*****************************");

		dataTableJSON.setiTotalDisplayRecords(count);
		dataTableJSON.setiTotalRecords(count);
		List<CompanyDto> list = companyService.findByFilter(dataTableAttributes, userContext.getUsername(), role);
		dataTableJSON.setAaData(list);
		logger.info("********************************end company controller end*****************************");

		return dataTableJSON;
	}
}
