package com.kingtransport.controller;

import com.kingtransport.dto.EmailDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author Mandeep Singh Gill
 * <p>
 * Controller created to send messages with email ad subjects
 */

@RestController
@RequestMapping(value = "/api/email")
public class EmailController {

    Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    @PostMapping(value = "/send")
    public ResponseDto getContact(@RequestBody final EmailDto emailDto, HttpServletRequest req) {
        logger.info("***************************Email Send Controller Start***********************");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", emailDto.getName());
        map.put("subject", emailDto.getSubject());
        map.put("phoneNumber", emailDto.getPhoneNumber());
        map.put("body", emailDto.getBody());
        logger.info("***************************Email Send Controller Ends***********************");
        return emailService.sendMail(emailDto.getEmails(), map);
    }
}
