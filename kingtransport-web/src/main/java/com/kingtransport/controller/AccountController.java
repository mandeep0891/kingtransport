package com.kingtransport.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.datatables.DataTableJSON;
import com.kingtransport.dto.AccountDetailDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.AccountDetailService;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.RoleMasterService;
import com.kingtransport.tools.IConstants;

@RestController
@RequestMapping(value = "/api/account")
public class AccountController {
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountDetailService accountDetailService;

	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private RoleMasterService roleMasterService;

	@RequestMapping(value = "/role", method = { RequestMethod.GET })
	public ResponseDto role(Authentication authentication, HttpServletRequest request) {
		logger.info("Select role");
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		return roleMasterService.selectRole(userContext.getUsername(), role);
	}

	@RequestMapping(value = "/company", method = { RequestMethod.GET })
	public ResponseDto selectDto(Authentication authentication, HttpServletRequest request) {
		String selectedRole = request.getParameter("selectedRole");
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		return companyService.selectCompany(userContext.getUsername(), role, selectedRole);
	}
	
	/*
	 * create new account only by Admin/Founder/Employee
	 */
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	public ResponseDto create(@RequestBody final AccountDetailDto accountDetailDto, Authentication authentication) {
		logger.info("Inside account create Detail");
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		if (!role.equals(IConstants.Role.ADMIN_ROLE.getValue()) && !role.equals(IConstants.Role.FOUNDER_ROLE.getValue())
				&& !role.equals(IConstants.Role.EMPLYEE_ROLE.getValue())) {
			return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					IConstants.ACCESS_DENIED);
		} else {
			return accountDetailService.save(accountDetailDto, userContext.getUsername());
		}
	}

	/*
	 * update Account only by Admin/Founder/Employee
	 */
	@RequestMapping(value = "/update", method = { RequestMethod.PUT })
	public ResponseDto update(@RequestBody final AccountDetailDto accountDetailDto, Authentication authentication) {
		logger.info("Inside account update Detail");
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		if (!role.equals(IConstants.Role.ADMIN_ROLE.getValue()) && !role.equals(IConstants.Role.FOUNDER_ROLE.getValue())
				&& !role.equals(IConstants.Role.EMPLYEE_ROLE.getValue())) {
			return new ResponseDto(HttpStatus.EXPECTATION_FAILED, HttpStatus.EXPECTATION_FAILED.value(), 2,
					IConstants.ACCESS_DENIED);
		} else {
			return accountDetailService.save(accountDetailDto, userContext.getUsername());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/paging", method = { RequestMethod.GET }, produces = "application/json")
	public DataTableJSON list(Authentication authentication, HttpServletRequest request) {
		logger.info("********************************start company controller*****************************");
		String length = request.getParameter("length");
		String pageStart = request.getParameter("start");
		String value = request.getParameter("searchParam");
		DataTableJSON dataTableJSON = new DataTableJSON();
		DataTableAttributes dataTableAttributes = new DataTableAttributes(Integer.valueOf(pageStart),
				Integer.valueOf(length), null, null, value);
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		System.out.println(role);
		Long count = accountDetailService.findCountByFilter(dataTableAttributes, userContext.getUsername(), role);
		logger.info("********************************count " + count + "*****************************");

		dataTableJSON.setiTotalDisplayRecords(count);
		dataTableJSON.setiTotalRecords(count);
		List<AccountDetailDto> list = accountDetailService.findByFilter(dataTableAttributes, userContext.getUsername(),
				role);
		dataTableJSON.setAaData(list);
		logger.info("********************************end company controller end*****************************");

		return dataTableJSON;
	}

}
