package com.kingtransport.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.datatables.DataTableJSON;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.dto.TruckDto;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.TruckService;

@RestController
@RequestMapping(value = "/api/trucks")
public class TruckController {

	private static final Logger logger = LoggerFactory.getLogger(TruckController.class);

	@Autowired
	private TruckService truckService;
	
	@Autowired
	private CompanyService companyService;

	@RequestMapping(value = "/company", method = { RequestMethod.GET })
	public ResponseDto selectDto(Authentication authentication, HttpServletRequest request) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		return companyService.selectCompany(userContext.getUsername(), role);
	}

	@PostMapping(value = "/create")
	public ResponseDto createTruck(@RequestBody TruckDto truckDto) {
		return truckService.createTruck(truckDto);
	}

	/**
	 * Server Side Processing for Student
	 *
	 * @param authentication
	 * @param request
	 * @return ResponseDTO
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/paging", method = { RequestMethod.GET }, produces = "application/json")
	public DataTableJSON list(Authentication authentication, HttpServletRequest request) {
		logger.info("********************************start truck controller*****************************");
		String length = request.getParameter("length");
		String pageStart = request.getParameter("start");
		String value = request.getParameter("searchParam");
		DataTableJSON dataTableJSON = new DataTableJSON();
		DataTableAttributes dataTableAttributes = new DataTableAttributes(Integer.valueOf(pageStart),
				Integer.valueOf(length), null, null, value);
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		System.out.println(role);
		Long count = truckService.findCountByFilter(dataTableAttributes, userContext.getUsername(), role);
		logger.info("********************************count " + count + "*****************************");
		dataTableJSON.setiTotalDisplayRecords(count);
		dataTableJSON.setiTotalRecords(count);
		List<TruckDto> list = truckService.findByFilter(dataTableAttributes, userContext.getUsername(), role);
		dataTableJSON.setAaData(list);
		logger.info("********************************end truck controller end*****************************");

		return dataTableJSON;
	}

	@PutMapping(value = "/update")
	public ResponseDto updateTruck(@RequestBody TruckDto truckDto) {
		return truckService.createTruck(truckDto);
	}
}