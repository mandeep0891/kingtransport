package com.kingtransport.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kingtransport.dto.ResponseDto;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.RoleMasterService;

@RestController
@RequestMapping(value = "/api/role")
public class RoleMasterController {

	private static final Logger logger = LoggerFactory.getLogger(RoleMasterController.class);

	@Autowired
	private RoleMasterService roleMasterService;

	@RequestMapping(value = "/select", method = { RequestMethod.GET })
	public ResponseDto selectDto(Authentication authentication, HttpServletRequest request) {
		logger.info("Select role");
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		return roleMasterService.selectRole(userContext.getUsername(), role);
	}

}
