package com.kingtransport.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.datatables.DataTableJSON;
import com.kingtransport.dto.CargosDto;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.AccountDetailService;
import com.kingtransport.service.CargosService;
import com.kingtransport.service.CompanyService;
import com.kingtransport.service.TrailerService;
import com.kingtransport.service.TruckService;

@RestController
@RequestMapping(value = "/api/cargos")
public class CargosController {

	private static final Logger logger = LoggerFactory.getLogger(CargosController.class);

	@Autowired
	private CompanyService companyService;

	@Autowired
	private AccountDetailService accountDetailService;

	@Autowired
	private TrailerService trailerService;

	@Autowired
	private TruckService truckService;

	@Autowired
	private CargosService cargosService;

	@RequestMapping(value = "/company", method = { RequestMethod.GET })
	public ResponseDto company(Authentication authentication, HttpServletRequest request) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		return companyService.selectCompany(userContext.getUsername(), role);
	}

	@RequestMapping(value = "/driver", method = { RequestMethod.GET })
	public ResponseDto driver(Authentication authentication, HttpServletRequest request) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		return accountDetailService.selectAccount(userContext.getUsername(),
				Long.parseLong(request.getParameter("companyId")));

	}

	@RequestMapping(value = "/additional/driver", method = { RequestMethod.GET })
	public ResponseDto additionalDriver(Authentication authentication, HttpServletRequest request) {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		return accountDetailService.selectAdditionalAccount(userContext.getUsername(),
				Long.parseLong(request.getParameter("driverId")), Long.parseLong(request.getParameter("companyId")));

	}

	@RequestMapping(value = "/truck", method = { RequestMethod.GET })
	public ResponseDto truck(Authentication authentication, HttpServletRequest request) {
		return truckService.selectTruck(Long.parseLong(request.getParameter("companyId")));

	}

	@RequestMapping(value = "/trailer", method = { RequestMethod.GET })
	public ResponseDto trailer(Authentication authentication, HttpServletRequest request) {
		return trailerService.selectTrailer(Long.parseLong(request.getParameter("companyId")));

	}

	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	public ResponseDto create(@RequestBody final CargosDto cargosDto, Authentication authentication) {
		return cargosService.createCargos(cargosDto);
	}
	
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	public ResponseDto update(@RequestBody final CargosDto cargosDto, Authentication authentication) {
		return cargosService.createCargos(cargosDto);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/paging", method = { RequestMethod.GET }, produces = "application/json")
	public DataTableJSON list(Authentication authentication, HttpServletRequest request) {
		logger.info("********************************start cargos controller*****************************");
		String length = request.getParameter("length");
		String pageStart = request.getParameter("start");
		String searchParam = request.getParameter("searchParam");
		String searchType = request.getParameter("searchType");
		String companyId = request.getParameter("companyId");
		DataTableJSON dataTableJSON = new DataTableJSON();
		DataTableAttributes dataTableAttributes = new DataTableAttributes(Integer.valueOf(pageStart),
				Integer.valueOf(length), null, null, searchParam);
		UserContext userContext = (UserContext) authentication.getPrincipal();
		List<GrantedAuthority> roleList = userContext.getAuthorities();
		String role = roleList.get(0).getAuthority();
		System.out.println(role);
		
		Long count = cargosService.findCountByFilter(dataTableAttributes, userContext.getUsername(), role, searchType, companyId);
		logger.info("********************************count " + count + "*****************************");

		dataTableJSON.setiTotalDisplayRecords(count);
		dataTableJSON.setiTotalRecords(count);
		List<CargosDto> list = cargosService.findByFilter(dataTableAttributes, userContext.getUsername(), role, searchType, companyId);
		dataTableJSON.setAaData(list);
		logger.info("********************************end cargos controller end*****************************");

		return dataTableJSON;
	}

}
