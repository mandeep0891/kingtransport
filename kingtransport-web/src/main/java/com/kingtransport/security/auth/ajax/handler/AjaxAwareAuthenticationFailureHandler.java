package com.kingtransport.security.auth.ajax.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.error.handler.AuthMethodNotSupportedException;
import com.kingtransport.error.handler.InvalidJwtTokenException;
import com.kingtransport.error.handler.JwtExpiredTokenException;
import com.kingtransport.error.handler.LogoutException;
import com.kingtransport.error.handler.OTPException;
import com.kingtransport.error.handler.SignupLinkActivationException;
import com.kingtransport.tools.CustomHttpStatus;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         After failure authentication this class is executed
 *
 */
@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {
	private final ObjectMapper mapper;

	@Autowired
	public AjaxAwareAuthenticationFailureHandler(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException, ServletException {

		// response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		ResponseDto responseDTO = null;

		if (e instanceof BadCredentialsException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.AUTHENTICATION_FAILED.getStatusCode(),
					"Invalid username or password");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof JwtExpiredTokenException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.JWT_TOKEN_EXPIRED.getStatusCode(), "Token has expired");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof InvalidJwtTokenException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.INVALID_JWT_TOKEN.getStatusCode(), "Invalid Jwt token.");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof LogoutException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.JWT_TOKEN_EXPIRED.getStatusCode(), "Token has expired.");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof AuthMethodNotSupportedException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.AUTH_METHOD_NOT_SUPPORTED.getStatusCode(), "Authentication failed");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof AuthenticationServiceException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.AUTHENTICATION_CREDENTIALS_NOT_PROVIDED.getStatusCode(),
					"Username or Password not provided");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof OTPException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.OtpStatus.OTP_INVALID.getStatusCode(), "Invalid OTP.");
			mapper.writeValue(response.getWriter(), responseDTO);
		} else if (e instanceof SignupLinkActivationException) {
			responseDTO = new ResponseDto(HttpStatus.UNAUTHORIZED,
					CustomHttpStatus.Authentication.SIGNUP_ACTIVATION_LINK_AUTH_EXCEPTION.getStatusCode(),
					"Signup link is not activated yet. Please activate first.");
			mapper.writeValue(response.getWriter(), responseDTO);
		}
		mapper.writeValue(response.getWriter(), responseDTO);
	}
}
