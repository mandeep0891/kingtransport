package com.kingtransport.security.auth.ajax;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.kingtransport.dto.AccountDto;
import com.kingtransport.dto.RoleMasterDto;
import com.kingtransport.error.handler.SignupLinkActivationException;
import com.kingtransport.security.auth.ajax.filter.CustomUsernamePasswordAuthenticationToken;
import com.kingtransport.security.auth.model.UserContext;
import com.kingtransport.service.AccountService;
import com.kingtransport.service.RoleMasterService;
import com.kingtransport.tools.ToolBox;

/**
 * 
 * @author Mandeep Singh Gill
 *
 *         Custom authentication provider class is created to set user
 *         credentials to UsernamePasswordAuthenticationToken from database to
 *         authenticate after validating database credentials to credentials set
 *         in custom filter
 */

@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private AccountService accountService;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private RoleMasterService roleMasterService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Assert.notNull(authentication, "No authentication data provided");
		if (!(authentication instanceof CustomUsernamePasswordAuthenticationToken)) {
			throw new IllegalArgumentException("Only CustomAuthenticationManager is supported");
		}
		/*
		 * map authentication object to CustomUsernamePasswordAuthenticationToken to
		 * retrieved credentials passed in filter to authentication manager
		 */
		CustomUsernamePasswordAuthenticationToken authenticationToken = (CustomUsernamePasswordAuthenticationToken) authentication;
		String username = (String) authenticationToken.getPrincipal();
		String password = (String) authenticationToken.getCredentials();
		// String otp = (String) authenticationToken.getOtp();
		AccountDto accountDto = null;
		accountDto = accountService.findByUserName(username);
		if (ToolBox.isObjectEmpty(accountDto) || !encoder.matches(password, accountDto.getPassword())) {
			throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
		}
		if (!ToolBox.isObjectEmpty(accountDto) && !accountDto.getStatus().equals("ACTIVE")) {
			throw new SignupLinkActivationException("User is registered but not activated yet.");
		}
		List<RoleMasterDto> roles = roleMasterService.findRoleDtoByAccountId(accountDto.getId());
		if (ToolBox.isCollectionEmpty(roles)) {
			throw new InsufficientAuthenticationException("User has no roles assigned");
		}
		List<GrantedAuthority> authorities = roles.stream()
				.map(authority -> new SimpleGrantedAuthority(authority.getRoleName())).collect(Collectors.toList());

		/*
		 * map username and authorities Usercontext to set into
		 * UsernamePasswordAuthenticationToken to authenticate. Now we passed both type
		 * of credentials to authentication and after authentication it will send
		 * control to Success handler
		 */
		UserContext userContext = UserContext.create(username, authorities);
		return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
