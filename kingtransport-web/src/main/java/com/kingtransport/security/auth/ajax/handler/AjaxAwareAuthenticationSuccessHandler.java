package com.kingtransport.security.auth.ajax.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kingtransport.dto.ResponseDto;
import com.kingtransport.security.auth.model.JwtToken;
import com.kingtransport.security.auth.model.JwtTokenFactory;
import com.kingtransport.security.auth.model.UserContext;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         After successful authentication this custom class is used to create
 *         token with jwt token is created and sent to response
 *
 */

@Component
public class AjaxAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private final ObjectMapper mapper;
	private final JwtTokenFactory tokenFactory;

	@Autowired
	public AjaxAwareAuthenticationSuccessHandler(ObjectMapper objectMapper, JwtTokenFactory jwtTokenFactory) {
		this.mapper = objectMapper;
		this.tokenFactory = jwtTokenFactory;
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		UserContext userContext = (UserContext) authentication.getPrincipal();
		JwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("token", accessToken.getToken());
		tokenMap.put("roleName", "employee");
		// tokenMap.put("refreshToken", refreshToken.getToken());
		ResponseDto responseDTO = new ResponseDto();
		responseDTO.setData(tokenMap);
		responseDTO.setStatus(HttpStatus.OK);
		responseDTO.setStatusCode(HttpStatus.OK.value());
		responseDTO.setMessage("SUCCESS");
		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mapper.writeValue(response.getWriter(), responseDTO);
		clearAuthenticationAttributes(request);
	}

}
