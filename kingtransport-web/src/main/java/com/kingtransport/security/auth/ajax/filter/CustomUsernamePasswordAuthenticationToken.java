package com.kingtransport.security.auth.ajax.filter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * 
 * @author Mandeep Singh gill
 * 
 *         Custom class created to handle extra parameters for authentication
 *         like otp otherwise we can direct use
 *         UsernamePasswordAuthenticationToken
 *
 */
public class CustomUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomUsernamePasswordAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}

}
