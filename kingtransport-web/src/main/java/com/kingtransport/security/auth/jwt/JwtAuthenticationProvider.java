package com.kingtransport.security.auth.jwt;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.kingtransport.config.properties.JwtSettings;
import com.kingtransport.security.auth.model.JwtToken;
import com.kingtransport.security.auth.model.RawAccessJwtToken;
import com.kingtransport.security.auth.model.UserContext;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/**
 * An {@link AuthenticationProvider} implementation that will use provided
 * instance of {@link JwtToken} to perform authentication.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {
	private final JwtSettings jwtSettings;

	@Autowired
	public JwtAuthenticationProvider(JwtSettings jwtSettings) {
		this.jwtSettings = jwtSettings;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		/*
		 * get jwt credentials from authentication passed in jwt token authentication
		 * processing filter
		 */
		RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();
		/*
		 * Get header,body, issuer,signature after parsing token with token signing key
		 */
		Jws<Claims> jwsClaims = rawAccessToken.parseClaims(jwtSettings.getTokenSigningKey());
		/*
		 * get subject, scope and authorties from body
		 */
		String subject = jwsClaims.getBody().getSubject();
		List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
		List<GrantedAuthority> authorities = scopes.stream().map(authority -> new SimpleGrantedAuthority(authority))
				.collect(Collectors.toList());
		/*
		 * create user context object from subject and authorities
		 */
		UserContext context = UserContext.create(subject, authorities);
		/*
		 * authenticate jwt credentials
		 */
		return new JwtAuthenticationToken(context, context.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
