package com.kingtransport.security.auth.model;

/**
 * Scopes
 * 
 * @author Mandeep Singh Gill
 *
 */
public enum Scopes {
	REFRESH_TOKEN;

	public String authority() {
		return "ROLE_" + this.name();
	}
}
