package com.kingtransport.security.auth.model;
/**
 * 
 * @author Mandeep Singh Gill
 *
 */
public interface JwtToken {
    String getToken();
}
