package com.kingtransport.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         class to validate the request
 *
 */
public class WebUtil {
	private static final Logger logger = LoggerFactory.getLogger(WebUtil.class);
	private static final String XML_HTTP_REQUEST = "XMLHttpRequest";
	private static final String X_REQUESTED_WITH = "X-Requested-With";

	public static boolean isAjax(HttpServletRequest request) {
		logger.debug("In Web Util Request header :::: " + request.getHeader("X_REQUESTED_WITH"));
		return XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH));
	}

}
