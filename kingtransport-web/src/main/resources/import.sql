
#INSERT company_type
INSERT INTO company_type(name, status, description) VALUES ('Transport', 'ACTIVE', 'Trucking Transport');

#INSERT address
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');
INSERT INTO address(address1, address2, address3, city, state, country, postal_code) VALUES ('10302 SE ', '230TH PL', 'KENT', 'KENT', 'WA', 'US', '98031');


#RESOURCES
INSERT INTO resources(resource_type, resource_ext, resource_path, table_name, column_name, status) VALUES ('IMAGE', 'jpg', 'std_profile/king-transport-logo.jpg', 'COMPANY', 'resource_id', 'ACTIVE');
INSERT INTO resources(resource_type, resource_ext, resource_path, table_name, column_name, status) VALUES ('IMAGE', 'jpg', 'std_profile/mandeep-profile-image.jpg', 'ACCOUNT_DETAIL', 'profile_image', 'ACTIVE');
INSERT INTO resources(resource_type, resource_ext, resource_path, table_name, column_name, status) VALUES ('IMAGE', 'jpg', 'std_profile/amrik-profile-image.jpg', 'ACCOUNT_DETAIL', 'profile_image', 'ACTIVE');

#INSERT COMPANY
INSERT INTO company(created_date, modified_date, company_type_id, address_id, email, contact_number, status, name, mc_no, dot_no, tax_id, resource_id, created_by, modified_by ) VALUES (now(), now(), 1, 1, 'kingtransport@gmail.com', '+1 (206) 816-2494', 'ACTIVE', 'King Transport, Inc', 'mc2828', 'dot2828', 'tno2828', 1, 2, 2);
INSERT INTO company(created_date, modified_date, company_type_id, address_id, email, contact_number, status, name, mc_no, dot_no, tax_id, resource_id, created_by, modified_by ) VALUES (now(), now(), 1, 2, 'kingtransport@gmail.com', '+1 (206) 816-2494', 'ACTIVE', 'King Transport, Inc1', 'mc2828', 'dot2828', 'tno2828', 1, 2, 2);
INSERT INTO company(created_date, modified_date, company_type_id, address_id, email, contact_number, status, name, mc_no, dot_no, tax_id, resource_id, created_by, modified_by ) VALUES (now(), now(), 1, 3, 'kingtransport@gmail.com', '+1 (206) 816-2494', 'ACTIVE', 'King Transport, Inc2', 'mc2828', 'dot2828', 'tno2828', 1, 2, 2);
INSERT INTO company(created_date, modified_date, company_type_id, address_id, email, contact_number, status, name, mc_no, dot_no, tax_id, resource_id, created_by, modified_by ) VALUES (now(), now(), 1, 4, 'kingtransport@gmail.com', '+1 (206) 816-2494', 'ACTIVE', 'King Transport, Inc3', 'mc2828', 'dot2828', 'tno2828', 1, 2, 2);
INSERT INTO company(created_date, modified_date, company_type_id, address_id, email, contact_number, status, name, mc_no, dot_no, tax_id, resource_id, created_by, modified_by ) VALUES (now(), now(), 1, 5, 'kingtransport@gmail.com', '+1 (206) 816-2494', 'ACTIVE', 'King Transport, Inc4', 'mc2828', 'dot2828', 'tno2828', 1, 2, 2);




#INSERT ROLE
insert into role_master(name, description, status) values ('founder', 'Owner of the Organization', 'ACTIVE');
insert into role_master(name, description, status) values ('admin', 'Administrative support to either a team or individual.', 'ACTIVE');
insert into role_master(name, description, status) values ('employee', 'Employee of the company', 'ACTIVE');
insert into role_master(name, description, status) values ('driver', 'Driver of the company', 'ACTIVE');

#Account
insert into account(username, password, status) values ('mandeep-kingtransport', '$2a$10$T3t6RdRjcEwR44T775jsP.NvbgSKMi7x9r1ORQA9gnx86GE6JDfQe', 'ACTIVE');
insert into account(username, password, status) values ('amrik-kingtransport', '$2a$10$T3t6RdRjcEwR44T775jsP.NvbgSKMi7x9r1ORQA9gnx86GE6JDfQe', 'ACTIVE');
insert into account(username, password, status) values ('sohan-kingtransport', '$2a$10$T3t6RdRjcEwR44T775jsP.NvbgSKMi7x9r1ORQA9gnx86GE6JDfQe', 'ACTIVE');

#AccountDetail
insert into account_detail(created_date, modified_date, account_id, address_id, first_name, last_name, email, phone_number, is_archived, profile_image, created_by, modified_by, company_id ) values (now(), now(), 1, 6, 'Mandeep', 'Gill', 'mandeep.gill.java@gmail.com', '4259007987', '1', 2, 2, 2, 1);
insert into account_detail(created_date, modified_date, account_id, address_id, first_name, last_name, email, phone_number, is_archived, profile_image, created_by, modified_by, company_id ) values (now(), now(), 2, 7, 'Amrik', 'Pandher', 'amrik@gmail.com', '2068162494', '1', 3, 2, 2, 2);
insert into account_detail(created_date, modified_date, account_id, address_id, first_name, last_name, email, phone_number, is_archived, profile_image, created_by, modified_by, company_id ) values (now(), now(), 3, 8, 'Sohan', 'Pandher', 'sohan@gmail.com', '2068162419', '1', 3, 2, 2, 2);

#AccountRole
insert into account_role(account_id, role_id) values (1, 3);
insert into account_role(account_id, role_id) values (2, 1);
insert into account_role(account_id, role_id) values (3, 4);

#EmailTemplate
insert into email_template(id, content, name, subject) values (10000001, '<!doctype html><html><head><meta charset="utf-8"><title>King Transport</title></head><body> <div  style="background-color: #027862; width: 800px; padding: 20px; margin: 0 auto; font-family: ''Open Sans'', sans-serif;">  <style type="text/css">@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,700);</style>  <table width="100%">   <tr>   <td align="center"><span class="logo-lg"><b>King </b>Transport</span></td>   </tr>  </table>  <table width="100%" height="auto"   style="background-color: #fff; border-radius: 5px; margin-top: 20px; padding: 30px;"   cellpadding="0" cellspacing="0" height="400">   <tr valign="top">    <td height="89">     <p>      Hi    </p>     <p>:body</p>    </td>   </tr> <tr><td><p>Thank You</p><p>:name</p><p>:phoneNumber</p></td> </tr>  </table>  <table width="100%">   <tr>    <td align="center">     <p style="margin: 22px 0 0; font-size: 12px;">© Copyright 2020      King Transport, Inc. All Rights Resevered.</p>    </td>   </tr>  </table> </div></body></html>', 'King transport', 'King Transport');

