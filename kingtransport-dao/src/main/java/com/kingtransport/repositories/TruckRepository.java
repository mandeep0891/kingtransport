package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.Truck;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Created to perform Account model related activities
 *
 */

@Repository
@Transactional
public interface TruckRepository extends BaseRepository<Truck, Long> {

	@Query("select count(*) from Truck t where t.truckNo =:truckNo ")
	public int validateByTruckNo(@Param("truckNo") String truckNo);

	@Query("select count(*) from Truck t where t.truckNo =:truckNo and t.name =:name ")
	public int validateByTruckNoAndName(@Param("truckNo") String truckNo, @Param("name") String name);
	
	@Query("SELECT NEW com.kingtransport.dto.SelectDto"
            + "(t.id as id, "
            + "t.name as name) "
            + "FROM Truck t "
            + "INNER JOIN Company c ON t.company.id = c.id where c.id =:companyId ")
    public List<SelectDto> selectTruck(@Param("companyId") Long companyId);
	
}
