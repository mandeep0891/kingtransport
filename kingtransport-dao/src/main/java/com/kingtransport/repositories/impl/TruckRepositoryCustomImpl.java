package com.kingtransport.repositories.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.TruckDto;
import com.kingtransport.repositories.TruckRepositoryCustom;

/**
 * This implementation of custom repository of Client
 * 
 * @author Mandeep Singh Gill
 *
 */
@Transactional(readOnly = true)
@Repository("truckRepositoryCustom")
public class TruckRepositoryCustomImpl extends BaseRepositoryCustomImpl<TruckDto, Long>
		implements TruckRepositoryCustom {
	private static final Logger logger = LoggerFactory.getLogger(TruckRepositoryCustomImpl.class);

	@Override
	public List<TruckDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role) {
		StringBuilder queryBuilder = null;
		if (role.equals("employee")) {
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.TruckDto" 
					+ "(t.id as id, "
					+ "t.name as truckName, " 
					+ "t.truckNo as truckNo, " 
					+ "c.id as companyId, " 
					+ "c.name as companyName) "
					+ "FROM AccountDetail ad " 
					+ "INNER JOIN Account a ON ad.account.id = a.id "
					+ "INNER JOIN Company c ON ad.company.id = c.id "
					+ "INNER JOIN Truck t on c.id = t.company.id where a.userName = '" 
					+ userName
					+ "' and c.name like '%" 
					+ dataTableAttributes.getSearchParam() 
					+ "%' order by c.name asc");
		} else {
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.TruckDto" 
					+ "(t.id as id, "
					+ "t.name as truckName, " 
					+ "t.truckNo as truckNo, " 
					+ "c.id as companyId, " 
					+ "c.name as companyName) "
					+ "FROM Company c "
					+ "INNER JOIN Truck t on c.id = t.company.id where "
					+ "c.name like '%" 
					+ dataTableAttributes.getSearchParam() 
					+ "%' order by c.name asc");
		}
		return findByCustomAndDynamicQueryWithLimit(queryBuilder, dataTableAttributes);
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role) {
		StringBuilder customQuery = new StringBuilder();
		if (dataTableAttributes.isFilterSelected()) {
			// customQuery =
			// prepareDynamicQueryAccordingFilterSelection(dataTableAttributes,isQuery);
		} else {
			customQuery = prepareDynamicQueryCountsWithoutFilterSelection(dataTableAttributes, userName, role);
		}
		return count(customQuery);
	}

	private StringBuilder prepareDynamicQueryCountsWithoutFilterSelection(DataTableAttributes dataTableAttributes,
			String userName, String role) {

		StringBuilder customQuery = new StringBuilder();
		if (role.equals("employee")) {
			customQuery = new StringBuilder("SELECT count(*) FROM AccountDetail ad " 
					+ "INNER JOIN Account a ON ad.account.id = a.id "
					+ "INNER JOIN Company c ON ad.company.id = c.id "
					+ "INNER JOIN Truck t on c.id = t.company.id where a.userName = '" 
					+ userName
					+ "' and c.name like '%" 
					+ dataTableAttributes.getSearchParam() 
					+ "%' order by c.name asc");
		} else {
			customQuery = new StringBuilder("SELECT count(*) FROM Company c "
					+ "INNER JOIN Truck t on c.id = t.company.id where "
					+ "c.name like '%" 
					+ dataTableAttributes.getSearchParam() 
					+ "%' order by c.name asc"); 
		}
		logger.info(customQuery.toString());
		return customQuery;
	}
}