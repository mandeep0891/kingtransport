package com.kingtransport.repositories.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.AccountDetailDto;
import com.kingtransport.repositories.AccountRepositoryCustom;

/**
 * This implementation of custom repository of Client
 * 
 * @author Mandeep Singh Gill
 *
 */
@Transactional(readOnly = true)
@Repository("accountRepositoryCustom")
public class AccountRepositoryCustomImpl extends BaseRepositoryCustomImpl<AccountDetailDto, Long>
		implements AccountRepositoryCustom {
	private static final Logger logger = LoggerFactory.getLogger(AccountRepositoryCustomImpl.class);

	@Override
	public List<AccountDetailDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role, Long conmpanyId) {
		StringBuilder queryBuilder = null;
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.AccountDetailDto" 
					+ "(ad.id as id, a.id as accountId, addr.id as addressId, c.id as companyId, "
					+ "rm.id as roleid, ar.id  as accountRoleId,  ad.ssNo as ssNo, "
					+ "ad.firstName as firstName, ad.lastName as lastName, ad.email as email, "
					+ "ad.phoneNumber as phoneNumber, ad.phoneNumber1 as phoneNumber1, "
					+ "ad.profileImage as profileImage, addr.address1 as address1, "
					+ "addr.address2 as address2, addr.address3 as address3 , "
					+ "addr.city as city, addr.state as state, addr.country as country, "
					+ "addr.postalCode as postalCode, ad.dob as dob, "
					+ "a.userName as userName, rm.name as roleName, c.name as companyName) "
					+ "from AccountRole ar " 
					+ "inner join Account a on ar.account.id = a.id "
					+ "inner join RoleMaster rm on ar.roleMaster.id = rm.id "
					+ "inner join AccountDetail ad on ad.account.id = a.id "
					+ "inner join Address addr on ad.address.id = addr.id "
					+ "inner join Company c on ad.company.id = c.id ");
					if (role.equals("employee")) {
						queryBuilder.append("and c.id = '"+conmpanyId+"'");
					}
					queryBuilder.append("where c.name like '%" 
							+ dataTableAttributes.getSearchParam() 
							+ "%' order by c.name asc");
		return findByCustomAndDynamicQueryWithLimit(queryBuilder, dataTableAttributes);
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role, Long conmpanyId) {
		StringBuilder customQuery = new StringBuilder();
		if (dataTableAttributes.isFilterSelected()) {
			// customQuery =
			// prepareDynamicQueryAccordingFilterSelection(dataTableAttributes,isQuery);
		} else {
			customQuery = prepareDynamicQueryCountsWithoutFilterSelection(dataTableAttributes, userName, role, conmpanyId);
		}
		return count(customQuery);
	}

	private StringBuilder prepareDynamicQueryCountsWithoutFilterSelection(DataTableAttributes dataTableAttributes,
			String userName, String role, Long conmpanyId) {

		StringBuilder customQuery = new StringBuilder();
			customQuery = customQuery.append("SELECT count(*) from AccountRole ar " 
					+ "inner join Account a on ar.account.id = a.id "
					+ "inner join RoleMaster rm on ar.roleMaster.id = rm.id "
					+ "inner join AccountDetail ad on ad.account.id = a.id "
					+ "inner join Address addr on ad.address.id = addr.id "
					+ "inner join Company c on ad.company.id = c.id ");
					if (role.equals("employee")) {
						customQuery.append("and c.id = '"+conmpanyId+"'");
					}
					customQuery.append("where c.name like '%" 
							+ dataTableAttributes.getSearchParam() 
							+ "%' order by c.name asc");
	logger.info(customQuery.toString());
		return customQuery;
	}
}