package com.kingtransport.repositories.impl;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.repositories.BaseRepositoryCustom;
import com.kingtransport.tools.ToolBox;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 * This is base repository custom implementation if you need some generic or
 * common methods then you should specify here.
 * 
 * @author Mandeep Singh Gill
 * @param <T>
 *
 */
@SuppressWarnings("unchecked")
@NoRepositoryBean
abstract public class BaseRepositoryCustomImpl<T, ID extends Serializable> implements BaseRepositoryCustom<T, ID> {

	@PersistenceContext
	@Qualifier(value = "entityManagerFactory")
	protected EntityManager em;

	/**
	 * setEntityManager
	 * 
	 * @param em
	 */
	protected void setEntityManager(EntityManager em) {
		this.em = em;
	}

	/**
	 * returning getCurrentSession
	 * 
	 * @return
	 */
	protected Session getCurrentSession() {

		return em.unwrap(Session.class);
	}

	@Override
	public List<T> findByCustomAndDynamicQuery(StringBuilder query) {
		return em.createQuery(query.toString()).getResultList();
	}
	
	@Override
	public List<T> findByCustomAndDynamicQueryWithLimit(StringBuilder query, DataTableAttributes dataTableAttributes) {
		return em.createQuery(query.toString()).setFirstResult(dataTableAttributes.getPageDisplayStart()).setMaxResults(dataTableAttributes.getPageDisplayLength()).getResultList();
	}

	@Override
	public Long count(StringBuilder customQuery) {
		Long count = 0l;
		Query query = em.createQuery(customQuery.toString(), Long.class);
		List<Object> objects = query.getResultList();
		if (!ToolBox.isCollectionEmpty(objects) && objects.size() > 1) {
			count = Long.valueOf(objects.size());
		} else if (!ToolBox.isCollectionEmpty(objects) && objects.size() == 1) {
			count = (Long) objects.get(0);
		}
		return count;
	}

}
