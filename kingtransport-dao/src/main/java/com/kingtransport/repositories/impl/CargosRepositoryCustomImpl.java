package com.kingtransport.repositories.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.CargosDto;
import com.kingtransport.repositories.CargosRepositoryCustom;

/**
 * This implementation of custom repository of Client
 * 
 * @author Mandeep Singh Gill
 *
 */
@Transactional(readOnly = true)
@Repository("cargosRepositoryCustom")
public class CargosRepositoryCustomImpl extends BaseRepositoryCustomImpl<CargosDto, Long>
		implements CargosRepositoryCustom {
	private static final Logger logger = LoggerFactory.getLogger(CargosRepositoryCustomImpl.class);

	@Override
	public List<CargosDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role, String searchType, String companyId) {
		StringBuilder queryBuilder = null;
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.CargosDto" 
					+ "(cargos.id as id, "
					+ "company.id as companyId, " 
					+ "account.id as driverId, " 
					+ "additionalAccount.id as additionalDriverId, "
					+ "truck.id as truckId, " 
					+ "trailer.id as trailerId, " 
					+ "cargos.noOfCargos as noOfCargos, " 
					+ "cargos.totalBilableHours as totalBilableHours, " 
					+ "company.name as companyName, " 
					+ "concat(accountDetail.lastName,', ',accountDetail.firstName) as driverName, " 
					+ "concat(additionalAccountDetail.lastName,', ',additionalAccountDetail.firstName) as additionalDriverName, " 
					+ "truck.name as truckName, " 
					+ "trailer.name as trailerName) "
					+ "FROM Cargos cargos "
					+ "inner join Company company on cargos.company.id = company.id "
					+ "inner join Truck truck on cargos.truck.id = truck.id "
					+ "inner join Trailer trailer on cargos.trailer.id = trailer.id "
					+ "inner join Account account on cargos.driver.id = account.id "
					+ "inner join AccountDetail accountDetail on account.id = accountDetail.account.id "
					+ "inner join Account additionalAccount on cargos.additionalDriver.id = additionalAccount.id "
					+ "inner join AccountDetail additionalAccountDetail on additionalAccount.id = additionalAccountDetail.account.id where 1=1 ");
			if(searchType.equals("All")) {
			}else if(searchType.equals("Driver")) {
				queryBuilder.append(" and concat(accountDetail.lastName,', ',accountDetail.firstName) like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("AdditionalDriver")) {
				queryBuilder.append(" and concat(additionalAccountDetail.lastName,', ',additionalAccountDetail.firstName) like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("Truck")) {
				queryBuilder.append(" and truck.name like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("Trailer")) {
				queryBuilder.append(" and trailer.name like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}
			queryBuilder.append(" and cargos.company.id = '"+companyId+"' order by company.name asc");
			return findByCustomAndDynamicQueryWithLimit(queryBuilder, dataTableAttributes);
	}
	

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role, String searchType,  String companyId) {
		StringBuilder customQuery = new StringBuilder();
		if (dataTableAttributes.isFilterSelected()) {
			// customQuery =
			// prepareDynamicQueryAccordingFilterSelection(dataTableAttributes,isQuery);
		} else {
			customQuery = prepareDynamicQueryCountsWithoutFilterSelection(dataTableAttributes, userName, role, searchType, companyId);
		}
		return count(customQuery);
	}

	private StringBuilder prepareDynamicQueryCountsWithoutFilterSelection(DataTableAttributes dataTableAttributes,
			String userName, String role,  String searchType, String companyId) {

		StringBuilder customQuery = new StringBuilder();
			customQuery = new StringBuilder("SELECT count(*) FROM Cargos cargos "
					+ "inner join Company company on cargos.company.id = company.id "
					+ "inner join Truck truck on cargos.truck.id = truck.id "
					+ "inner join Trailer trailer on cargos.trailer.id = trailer.id "
					+ "inner join Account account on cargos.driver.id = account.id "
					+ "inner join AccountDetail accountDetail on account.id = accountDetail.account.id "
					+ "inner join Account additionalAccount on cargos.additionalDriver.id = additionalAccount.id "
					+ "inner join AccountDetail additionalAccountDetail on additionalAccount.id = additionalAccountDetail.account.id "
					+ "where 1=1 ");
			
			if(searchType.equals("All")) {
			}else if(searchType.equals("Driver")) {
				customQuery.append(" and concat(accountDetail.lastName,', ',accountDetail.firstName) like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("AdditionalDriver")) {
				customQuery.append(" and concat(additionalAccountDetail.lastName,', ',additionalAccountDetail.firstName) like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("Truck")) {
				customQuery.append(" and truck.name like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}else if(searchType.equals("Trailer")) {
				customQuery.append(" and trailer.name like '%" + dataTableAttributes.getSearchParam() + "%' ");
			}
			customQuery.append(" and cargos.company.id = '"+companyId+"' order by company.name asc");
		logger.info(customQuery.toString());
		return customQuery;
	}
}