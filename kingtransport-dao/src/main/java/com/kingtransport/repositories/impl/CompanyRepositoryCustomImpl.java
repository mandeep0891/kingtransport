package com.kingtransport.repositories.impl;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.CompanyDto;
import com.kingtransport.repositories.CompanyRepositoryCustom;
import com.kingtransport.tools.IConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * This implementation of custom repository of Client
 * 
 * @author Mandeep Singh Gill
 *
 */
@Transactional(readOnly = true)
@Repository("companyRepositoryCustom")
public class CompanyRepositoryCustomImpl extends BaseRepositoryCustomImpl<CompanyDto, Long>
		implements CompanyRepositoryCustom {
	private static final Logger logger = LoggerFactory.getLogger(CompanyRepositoryCustomImpl.class);

	@Override
	public List<CompanyDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role) {
		StringBuilder queryBuilder = null;
		if (role.equals(IConstants.Role.EMPLYEE_ROLE.getValue())) {
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.CompanyDto(c.id as id, "
					+ "ct.id as companyTypeId, adr.id as addressId, ct.name as companyType, c.name as companyName, "
					+ "c.taxId as taxId, c.mcNo as mcNo, c.dotNo as dotNo, c.email as email, c.contactNumber as contactNumber, "
					+ "adr.address1 as address1, adr.address2 as address2, adr.address3 as address3, adr.city as city, adr.country as country, adr.postalCode as postalCode, adr.state as state) "
					+ "FROM AccountDetail ad INNER JOIN Account a ON ad.account.id = a.id "
					+ "INNER JOIN Company c ON ad.company.id = c.id "
					+ "INNER JOIN CompanyType ct on ct.id = c.companyType.id INNER JOIN Address adr on adr.id = c.address.id where a.userName = '" + userName
					+ "' and c.name like '%" + dataTableAttributes.getSearchParam() + "%' order by c.name asc");
		} else {
			queryBuilder = new StringBuilder("SELECT NEW com.kingtransport.dto.CompanyDto(c.id as id, "
					+ "ct.id as companyTypeId, adr.id as addressId, ct.name as companyType, c.name as companyName, "
					+ "c.taxId as taxId, c.mcNo as mcNo, c.dotNo as dotNo, c.email as email, c.contactNumber as contactNumber, "
					+ "adr.address1 as address1, adr.address2 as address2, adr.address3 as address3, adr.city as city, adr.country as country, adr.postalCode as postalCode, adr.state as state) "
					+ "FROM Company c "
					+ "INNER JOIN CompanyType ct on ct.id = c.companyType.id INNER JOIN Address adr on adr.id = c.address.id where "
					+ "c.name like '%" + dataTableAttributes.getSearchParam() + "%' order by c.name asc");
		}
		return findByCustomAndDynamicQueryWithLimit(queryBuilder, dataTableAttributes);
	}

	@Override
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role) {
		StringBuilder customQuery = new StringBuilder();
		if (dataTableAttributes.isFilterSelected()) {
			// customQuery =
			// prepareDynamicQueryAccordingFilterSelection(dataTableAttributes,isQuery);
		} else {
			customQuery = prepareDynamicQueryCountsWithoutFilterSelection(dataTableAttributes, userName, role);
		}
		return count(customQuery);
	}

	private StringBuilder prepareDynamicQueryCountsWithoutFilterSelection(DataTableAttributes dataTableAttributes,
			String userName, String role) {

		StringBuilder customQuery = new StringBuilder();
		if (role.equals(IConstants.Role.EMPLYEE_ROLE.getValue())) {
			customQuery = customQuery.append("SELECT count(*) FROM AccountDetail ad "
					+ "INNER JOIN Account a ON ad.account.id = a.id " + "INNER JOIN Company c ON ad.company.id = c.id "
					+ "INNER JOIN CompanyType ct on ct.id = c.companyType.id INNER JOIN Address adr on adr.id = c.address.id where  a.userName = '" + userName
					+ "' and c.name like '%" + dataTableAttributes.getSearchParam() + "%' order by c.name asc");
		} else {
			customQuery = customQuery.append("SELECT count(*) FROM Company c "
					+ "INNER JOIN CompanyType ct on ct.id = c.companyType.id INNER JOIN Address adr on adr.id = c.address.id where "
					+ "c.name like '%" + dataTableAttributes.getSearchParam() + "%' order by c.name asc");
		}
		logger.info(customQuery.toString());
		return customQuery;
	}
}