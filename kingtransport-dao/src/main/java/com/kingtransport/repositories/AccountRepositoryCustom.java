/**
 *
 */
package com.kingtransport.repositories;

import java.util.List;

import com.kingtransport.datatables.DataTableAttributes;
import com.kingtransport.dto.AccountDetailDto;

/**
 * This repository is making or handling dynamic or customize query specially
 * for Data tables .
 *
 * @author Mandeep Singh Gill
 *
 */
public interface AccountRepositoryCustom extends BaseRepositoryCustom<AccountDetailDto, Long> {

	/**
	 * dynamic query is prepared in the implementation class.
	 *
	 * @param dataTableAttributes
	 * @return
	 */
	public List<AccountDetailDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role, Long companyId);

	/**
	 * This function returning number of count according dynamic query.
	 *
	 * @param dataTableAttributes
	 * @return
	 */
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role, Long companyId);

}
