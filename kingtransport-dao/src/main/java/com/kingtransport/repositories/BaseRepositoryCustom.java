package com.kingtransport.repositories;

import com.kingtransport.datatables.DataTableAttributes;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

/**
 * This repository is making or handling dynamic or customize query specially
 * for Data tables where we getting dynamic page number, .
 * 
 * @author Mandeep Singh Gill
 *
 */
@NoRepositoryBean
public interface BaseRepositoryCustom<T, ID extends Serializable> {
	/**
	 * This function is generic for custom query.
	 * 
	 * @param query
	 */
	public abstract List<T> findByCustomAndDynamicQuery(StringBuilder query);

	/**
	 * returning only count according custom dynamic query.
	 * 
	 * @param customQuery
	 * @return
	 */
	public Long count(StringBuilder customQuery);

	/**
	 * Ths query will return list of data on the basis of custom query with Limit
	 * (index based).
	 * 
	 * @param query
	 * @param dataTableAttributes
	 * @return
	 */
	public List<T> findByCustomAndDynamicQueryWithLimit(StringBuilder query, DataTableAttributes dataTableAttributes);
}