package com.kingtransport.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kingtransport.entities.Cargos;

public interface CargosRepository extends BaseRepository<Cargos, Long> {

	@Query("SELECT count(*) FROM Cargos cargos INNER JOIN Company company on cargos.company.id = company.id "
			+ "INNER JOIN Account account on cargos.driver.id = account.id "
			+ "INNER JOIN  Account addAccount on cargos.additionalDriver.id = addAccount.id "
			+ "INNER JOIN Truck truck on cargos.truck.id = truck.id  "
			+ "INNER JOIN Trailer trailer on cargos.trailer.id = trailer.id where company.id=:companyId and  account.id =:driverId and "
			+ "addAccount.id =:additionalDriver and truck.id =:truckId and trailer.id =:trailerId  ")
	public int findCargoByCompanyIdAndDriverIdAndADriverIdAndTruckIdAndTrailerId(@Param("companyId") Long companyId,
			@Param("driverId") Long driverId, @Param("additionalDriver") Long additionalDriver,
			@Param("truckId") Long truckId, @Param("trailerId") Long trailerId);

}
