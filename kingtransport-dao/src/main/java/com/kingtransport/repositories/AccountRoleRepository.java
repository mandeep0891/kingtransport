package com.kingtransport.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.entities.AccountRole;

/**
 * 
 * @author Mandeep Singh Gill
 *
 * 
 */
@Repository
@Transactional
public interface AccountRoleRepository extends BaseRepository<AccountRole, Long> {

}
