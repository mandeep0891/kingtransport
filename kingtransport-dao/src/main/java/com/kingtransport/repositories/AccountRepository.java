package com.kingtransport.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.AccountDto;
import com.kingtransport.entities.Account;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Created to perform Account model related activities
 *
 */

@Repository
@Transactional
public interface AccountRepository extends BaseRepository<Account, Long> {

	/**
	 * This query is use to validate user R
	 * 
	 * @param username
	 * @return
	 */
	@Query("SELECT NEW com.kingtransport.dto.AccountDto" + "(ac.id , " + "ac.userName, " + "ac.password, "
			+ "ac.status)" + "FROM AccountDetail acd INNER JOIN acd.account ac " + "WHERE  ac.userName=:username")
	public AccountDto findAccountDTOByUserName(@Param("username") String username);

	public Account findByUserName(String userName);

}
