package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.CompanyDto;
import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.Company;

/**
 * @author Mandeep Singh Gill
 * <p>
 * Created to perform Company related activities
 */

@Repository
@Transactional
public interface CompanyRepository extends BaseRepository<Company, Long> {
	
    @Query("SELECT NEW com.kingtransport.dto.SelectDto"
            + "(c.id as id, "
            + "c.name as companyName) "
            + "FROM AccountDetail ad "
            + "INNER JOIN Account a ON ad.account.id = a.id "
            + "INNER JOIN Company c ON ad.company.id = c.id where a.userName =:username ")
    public List<SelectDto> selectCompanyByUsername(@Param("username") String username);
    
    @Query("SELECT NEW com.kingtransport.dto.SelectDto"
            + "(c.id as id, "
            + "c.name as companyName) "
            + "FROM Company c")
    public List<SelectDto> selectCompany();
    /**
     * Query to get company Detail
     *
     * @param username
     * @return
     */
    @Query("SELECT NEW com.kingtransport.dto.CompanyDto"
            + "(c.id as id, "
            + "ct.name as companyType, "
            + "c.name as companyName, "
            + "c.taxId as taxId, "
            + "c.mcNo as mcNo, "
            + "c.dotNo as dotNo, "
            + "c.email as email, "
            + "c.contactNumber as contactNumber) "
            + "FROM AccountDetail ad "
            + "INNER JOIN Account a ON ad.account.id = a.id "
            + "INNER JOIN Company c ON ad.company.id = c.id "
            + "INNER JOIN CompanyType ct on ct.id = c.companyType.id where a.userName =:username ")
    public CompanyDto findACompanyDtoByUserName(@Param("username") String username);
    
    @Query("SELECT NEW com.kingtransport.dto.CompanyDto"
            + "(c.id as id, "
            + "ct.name as companyType, "
            + "ct.id as companyTypeId, "
            + "c.name as companyName, "
            + "c.taxId as taxId, "
            + "c.mcNo as mcNo, "
            + "c.dotNo as dotNo, "
            + "c.email as email, "
            + "c.contactNumber as contactNumber) "
            + "FROM Company c "
            + "INNER JOIN CompanyType ct on ct.id = c.companyType.id where c.id =:id ")
    public CompanyDto findACompanyDtoById(@Param("id") Long id);

}
