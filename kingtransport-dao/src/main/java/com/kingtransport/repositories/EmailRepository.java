package com.kingtransport.repositories;

import com.kingtransport.entities.EmailTemplate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * EmailTemplate model related activities.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Repository
public interface EmailRepository extends BaseRepository<EmailTemplate, Long> {

	@Query("select et from EmailTemplate et where et.id =:id ")
	EmailTemplate findTemplateById(@Param("id") Long id);

}
