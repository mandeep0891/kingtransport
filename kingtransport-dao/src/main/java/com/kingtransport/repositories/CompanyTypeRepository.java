package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.CompanyType;

/**
 * @author Mandeep Singh Gill
 *         <p>
 *         Created to perform Company related activities
 */

@Repository
@Transactional
public interface CompanyTypeRepository extends BaseRepository<CompanyType, Long> {
	
	
	/*
	 * R
	 */
	 @Query("SELECT NEW com.kingtransport.dto.SelectDto"
	            + "(c.id as id, "
	            + "c.name as companyType) "
	            + "FROM CompanyType c")
	    public List<SelectDto> selectCompanyType();

}
