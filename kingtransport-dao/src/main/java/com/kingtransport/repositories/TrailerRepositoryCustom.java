/**
 *
 */
package com.kingtransport.repositories;

import java.util.List;

import com.kingtransport.dto.TrailerDto;
import com.kingtransport.datatables.DataTableAttributes;

/**
 * This repository is making or handling dynamic or customize query specially
 * for Data tables .
 *
 * @author Mandeep Singh Gill
 *
 */
public interface TrailerRepositoryCustom extends BaseRepositoryCustom<TrailerDto, Long> {

	/**
	 * dynamic query is prepared in the implementation class.
	 *
	 * @param dataTableAttributes
	 * @return
	 */
	public List<TrailerDto> findByFilter(DataTableAttributes dataTableAttributes, String userName, String role);

	/**
	 * This function returning number of count according dynamic query.
	 *
	 * @param dataTableAttributes
	 * @return
	 */
	public Long findCountByFilter(DataTableAttributes dataTableAttributes, String userName, String role);

}
