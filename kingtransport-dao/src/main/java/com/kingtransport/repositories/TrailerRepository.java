package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.Trailer;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Created to perform Account model related activities
 *
 */

@Repository
@Transactional
public interface TrailerRepository extends BaseRepository<Trailer, Long> {

	@Query("select count(*) from Trailer t where t.trailerNo =:trailerNo ")
	public int validateByTrailerNo(@Param("trailerNo") String trailerNo);

	@Query("select count(*) from Trailer t where t.trailerNo =:trailerNo and t.name =:name ")
	public int validateByTrailerNoAndName(@Param("trailerNo") String trailerNo, @Param("name") String name);
	
	@Query("SELECT NEW com.kingtransport.dto.SelectDto"
            + "(t.id as id, "
            + "t.name as name) "
            + "FROM Trailer t "
            + "INNER JOIN Company c ON t.company.id = c.id where c.id =:companyId ")
    public List<SelectDto> selectTrailer(@Param("companyId") Long companyId);
}
