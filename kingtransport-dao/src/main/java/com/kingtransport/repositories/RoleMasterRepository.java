package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.RoleMasterDto;
import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.RoleMaster;

/**
 * 
 * @author Mandeep Singh Gill
 *
 * 
 */
@Repository
@Transactional
public interface RoleMasterRepository extends BaseRepository<RoleMaster, Long> {

	@Query("SELECT NEW com.kingtransport.dto.RoleMasterDto" + "(rm.id, rm.name, rm.status, rm.description)"
			+ "FROM AccountRole ar INNER JOIN  ar.roleMaster rm INNER JOIN ar.account a WHERE  a.id=:id")
	public List<RoleMasterDto> findRoleDTOByAccountId(@Param("id") Long id);

	public RoleMaster findByName(@Param("name") String name);

	@Query("SELECT NEW com.kingtransport.dto.SelectDto" + "(rm.id as id, " + "rm.name as roleName) "
			+ "FROM RoleMaster rm where rm.name in ('driver')")
	public List<SelectDto> selectRoleForDriver();

	@Query("SELECT NEW com.kingtransport.dto.SelectDto" + "(rm.id as id, " + "rm.name as roleName) "
			+ "FROM RoleMaster rm where rm.name in ('employee','driver')")
	public List<SelectDto> selectRole();
}
