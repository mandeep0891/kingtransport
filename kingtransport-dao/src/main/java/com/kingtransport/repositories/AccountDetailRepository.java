package com.kingtransport.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.dto.SelectDto;
import com.kingtransport.entities.AccountDetail;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Created to perform Account model related activities
 *
 */

@Repository
@Transactional
public interface AccountDetailRepository extends BaseRepository<AccountDetail, Long> {

	@Query("SELECT acd.id FROM AccountDetail acd INNER JOIN acd.account ac on ac.id = acd.account.id WHERE  ac.id=:id")
	public Long findAccountDTOByUserName(@Param("id") Long id);

	@Query("SELECT NEW com.kingtransport.dto.SelectDto" + "(a.id as id, "
			+ "concat(ad.lastName,', ',ad.firstName) as name) " + "FROM AccountDetail ad "
			+ "INNER JOIN Account a ON ad.account.id = a.id "
			+ "INNER JOIN Company c ON ad.company.id = c.id where c.id =:companyId and a.userName not in (:username) ")
	public List<SelectDto> selectAccount(@Param("username") String username, @Param("companyId") Long companyId);

	@Query("SELECT NEW com.kingtransport.dto.SelectDto" + "(a.id as id, "
			+ "concat(ad.lastName,', ',ad.firstName) as name) " + "FROM AccountDetail ad "
			+ "INNER JOIN Account a ON ad.account.id = a.id "
			+ "INNER JOIN Company c ON ad.company.id = c.id where c.id =:companyId and a.id not in (:accountId) and a.userName not in (:username) ")
	public List<SelectDto> selectAdditionalAccount(@Param("username") String username, @Param("accountId") Long accountId,
			@Param("companyId") Long companyId);

}
