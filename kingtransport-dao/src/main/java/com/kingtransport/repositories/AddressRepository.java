package com.kingtransport.repositories;

import java.util.Optional;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kingtransport.entities.Address;

/**
 * 
 * @author Mandeep Singh Gill
 * 
 *         Created to perform Account model related activities
 *
 */

@Repository
@Transactional
public interface AddressRepository extends BaseRepository<Address, Long> {

	
	public Optional<Address> findById(Long id);
	
}
