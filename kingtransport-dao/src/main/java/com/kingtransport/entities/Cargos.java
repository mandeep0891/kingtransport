package com.kingtransport.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.kingtransport.tools.IConstants.ColumnLength;

@Entity
@Table(name = "CARGOS")
@DynamicUpdate
public class Cargos implements Serializable {
	/*
	 * Java Serialization you can stream your Java object to a sequence of byte and
	 * restore these objects from this stream of bytes
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Company company;
	private Truck truck;
	private Trailer trailer;
	private Account driver;
	private Account additionalDriver;
	private int noOfCargos=0;
	private int totalBilableHours=0;

	public Cargos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cargos(Long id, Company company, Truck truck, Trailer trailer) {
		super();
		this.id = id;
		this.company = company;
		this.truck = truck;
		this.trailer = trailer;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID", foreignKey = @ForeignKey(name = "fk_company_cargos"), updatable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne
	@JoinColumn(name = "TRUCK_ID", foreignKey = @ForeignKey(name = "fk_truck_cargos"), updatable = false)
	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	@ManyToOne
	@JoinColumn(name = "TRAILER_ID", foreignKey = @ForeignKey(name = "fk_trailer_cargos"), updatable = false)
	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	@Column(name = "NO_OF_CARGOS", length = ColumnLength.SHORT_TEXT)
	public int getNoOfCargos() {
		return noOfCargos;
	}

	public void setNoOfCargos(int noOfCargos) {
		this.noOfCargos = noOfCargos;
	}

	@ManyToOne
	@JoinColumn(name = "DRIVER_ID", foreignKey = @ForeignKey(name = "fk_account_cargos"), updatable = false)
	public Account getDriver() {
		return driver;
	}

	public void setDriver(Account driver) {
		this.driver = driver;
	}

	@ManyToOne
	@JoinColumn(name = "ADDITIONAL_DRIVER_ID", foreignKey = @ForeignKey(name = "fk_additional_account_cargos"), updatable = false)
	public Account getAdditionalDriver() {
		return additionalDriver;
	}

	public void setAdditionalDriver(Account additionalDriver) {
		this.additionalDriver = additionalDriver;
	}

	@Column(name = "TOTAL_BILABLE_HOURS", length = ColumnLength.SHORT_TEXT)
	public int getTotalBilableHours() {
		return totalBilableHours;
	}

	public void setTotalBilableHours(int totalBilableHours) {
		this.totalBilableHours = totalBilableHours;
	}

}
