package com.kingtransport.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;

/**
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "ACCOUNT_DETAIL")
@DynamicUpdate
public class AccountDetail extends Base implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Account account;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String phoneNumber1;
	private long profileImage;
	private Address address;
	private Company company;
	private Date dob;
	private Long ssNo;

	public AccountDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ACCOUNT_ID", foreignKey = @ForeignKey(name = "fk_account_account_detail"), nullable = false)
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@OneToOne
	@JoinColumn(name = "ADDRESS_ID", foreignKey = @ForeignKey(name = "fk_address_account_detail"))
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID", foreignKey = @ForeignKey(name = "fk_company_account_detail"))
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "FIRST_NAME", length = 255, nullable = false)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME", length = 255, nullable = false)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "EMAIL", length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PHONE_NUMBER")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "PHONE_NUMBER_1")
	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	@Column(name = "PROFILE_IMAGE", length = 255)
	public long getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(long profileImage) {
		this.profileImage = profileImage;
	}

	@Column(name = "DOB")
	@Temporal(TemporalType.DATE)
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Column(name = "SS_NO", length = 255)
	public Long getSsNo() {
		return ssNo;
	}

	public void setSsNo(Long ssNo) {
		this.ssNo = ssNo;
	}

	
}
