package com.kingtransport.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kingtransport.tools.IConstants.ColumnLength;

/**
 * The class SchoolType
 * 
 * The @Entity annotation is a marker annotation, which is used to discover
 * persistent entities.
 * 
 * The @Table annotation is used to map the entity to the table in data base.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "COMPANY_TYPE")
public class CompanyType implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String description;
	private String status;

	public CompanyType() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * The @Id Specifies the primary key of the table.
	 * 
	 * The @GeneratedValue Provides for the specification of generation strategies
	 * for the values of primary keys.
	 * 
	 * @Column annotation allows us to define additional properties of the
	 * field/column, including length, whether it is nullable, whether it must be
	 * unique,
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "DESCRIPTION", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "STATUS", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
