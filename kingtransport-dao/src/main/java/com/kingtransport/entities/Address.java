package com.kingtransport.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.kingtransport.tools.IConstants.ColumnLength;

/**
 * The class Address
 * 
 * The @Entity annotation is a marker annotation, which is used to discover
 * persistent entities.
 * 
 * The @Table annotation is used to map the entity to the table in data base.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "ADDRESS")
@DynamicUpdate
public class Address implements Serializable {
	/*
	 * Java Serialization you can stream your Java object to a sequence of byte and
	 * restore these objects from this stream of bytes
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private Company company;
	private AccountDetail accountDetail;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(Long id) {
		super();
		this.id = id;
	}

	public Address(String address1, String address2, String address3, String city, String state, String country,
			String postalCode) {
		super();
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
	}

	/*
	 * The @Id Specifies the primary key of the table.
	 * 
	 * The @GeneratedValue Provides for the specification of generation strategies
	 * for the values of primary keys.
	 * 
	 * @Column annotation allows us to define additional properties of the
	 * field/column, including length, whether it is nullable, whether it must be
	 * unique,
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ADDRESS1", length = ColumnLength.LONG_TEXT, nullable = false)
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@Column(name = "ADDRESS2", length = ColumnLength.LONG_TEXT, nullable = true)
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@Column(name = "ADDRESS3", length = ColumnLength.LONG_TEXT, nullable = true)
	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	@Column(name = "CITY", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "STATE", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "COUNTRY", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "POSTAL_CODE", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/*
	 * FetchType.LAZY: It fetches the child entities lazily, that is, at the time of
	 * fetching parent entity it just fetches proxy (created by cglib or any other
	 * utility) of the child entities and when you access any property of child
	 * entity then it is actually fetched by hibernate.Doesn’t load the
	 * relationships unless explicitly “asked for” via getter
	 * 
	 * FetchType.EAGER: it fetches the child entities along with parent.Loads ALL
	 * relationships.
	 */
	@OneToOne(mappedBy = "address", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@OneToOne(mappedBy = "address", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public AccountDetail getAccountDetail() {
		return accountDetail;
	}

	public void setAccountDetail(AccountDetail accountDetail) {
		this.accountDetail = accountDetail;
	}

}
