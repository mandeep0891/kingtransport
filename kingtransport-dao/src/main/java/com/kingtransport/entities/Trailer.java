package com.kingtransport.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.kingtransport.tools.IConstants.ColumnLength;

@Entity
@Table(name = "TRAILER")
@DynamicUpdate
public class Trailer {

	private Long id;
	private String name;
	private String trailerNo;
	private Company company;
	private Set<Cargos> cargos = new HashSet<Cargos>();

	public Trailer() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "TRAILER_NO", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getTrailerNo() {
		return trailerNo;
	}

	public void setTrailerNo(String trailerNo) {
		this.trailerNo = trailerNo;
	}

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID", foreignKey = @ForeignKey(name = "fk_company_trailer"), updatable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@OneToMany(mappedBy = "trailer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Cargos> getCargos() {
		return cargos;
	}

	public void setCargos(Set<Cargos> cargos) {
		this.cargos = cargos;
	}
}
