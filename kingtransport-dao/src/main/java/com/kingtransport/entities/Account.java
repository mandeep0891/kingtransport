package com.kingtransport.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The class Account
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "ACCOUNT")
@DynamicUpdate
public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String userName;
	private String password;
	private String status;
	private Set<AccountDetail> accountDetails = new HashSet<AccountDetail>();
	private Set<AccountRole> accountRole = new HashSet<AccountRole>();
	private Set<Cargos> cargos = new HashSet<Cargos>();
	private Set<Cargos> cargos2 = new HashSet<Cargos>();
	

	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<AccountDetail> getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(Set<AccountDetail> accountDetails) {
		this.accountDetails = accountDetails;
	}

	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<AccountRole> getAccountRole() {
		return accountRole;
	}

	public void setAccountRole(Set<AccountRole> accountRole) {
		this.accountRole = accountRole;
	}

	public Account() {
		super();
	}

	public Account(Long id) {
		super();
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "USERNAME", length = 255, updatable = false)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "PASSWORD", length = 255, updatable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "STATUS", length = 255, updatable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "driver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Cargos> getCargos() {
		return cargos;
	}

	public void setCargos(Set<Cargos> cargos) {
		this.cargos = cargos;
	}

	@OneToMany(mappedBy = "additionalDriver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Cargos> getCargos2() {
		return cargos2;
	}

	public void setCargos2(Set<Cargos> cargos2) {
		this.cargos2 = cargos2;
	}
	
	
}
