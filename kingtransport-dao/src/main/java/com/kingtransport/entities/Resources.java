package com.kingtransport.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kingtransport.tools.IConstants.ColumnLength;

/**
 * The class Resources
 * 
 * The @Entity annotation is a marker annotation, which is used to discover
 * persistent entities.
 * 
 * The @Table annotation is used to map the entity to the table in data base.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "RESOURCES")
public class Resources implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String resourceType;
	private String resourceExt;
	private String resourcePath;
	private String tableName;
	private String columnName;
	private String status;

	public Resources() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Resources(long id) {
		super();
		this.id = id;
	}

	/*
	 * The @Id Specifies the primary key of the table.
	 * 
	 * The @GeneratedValue Provides for the specification of generation strategies
	 * for the values of primary keys.
	 * 
	 * @Column annotation allows us to define additional properties of the
	 * field/column, including length, whether it is nullable, whether it must be
	 * unique,
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "RESOURCE_TYPE", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@Column(name = "RESOURCE_EXT", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getResourceExt() {
		return resourceExt;
	}

	public void setResourceExt(String resourceExt) {
		this.resourceExt = resourceExt;
	}

	@Column(name = "RESOURCE_PATH", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	@Column(name = "TABLE_NAME", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Column(name = "COLUMN_NAME", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@Column(name = "STATUS", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
