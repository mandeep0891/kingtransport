package com.kingtransport.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.kingtransport.tools.IConstants.ColumnLength;

/**
 * The class School
 * 
 * The @Entity annotation is a marker annotation, which is used to discover
 * persistent entities.
 * 
 * The @Table annotation is used to map the entity to the table in data base.
 * 
 * @author Mandeep Singh Gill
 *
 */
@Entity
@Table(name = "COMPANY")
@DynamicUpdate
public class Company extends Base implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long resourceId;
	private CompanyType companyType;
	private Set<AccountDetail> accountDetails = new HashSet<AccountDetail>();
	private String email;
	private String contactNumber;
	private String status;
	private Address address;
	private String name;
	private String taxId;
	private String mcNo;
	private String dotNo;
	private Set<Trailer> trailers = new HashSet<Trailer>();
	private Set<Truck> trucks = new HashSet<Truck>();
	private Set<Cargos> cargos = new HashSet<Cargos>();

	public Company() {
		super();
	}

	public Company(Long id) {
		super();
		this.id = id;
	}

	/*
	 * The @Id Specifies the primary key of the table.
	 * 
	 * The @GeneratedValue Provides for the specification of generation strategies
	 * for the values of primary keys.
	 * 
	 * @Column annotation allows us to define additional properties of the
	 * field/column, including length, whether it is nullable, whether it must be
	 * unique,
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(name = "COMPANY_TYPE_ID", foreignKey = @ForeignKey(name = "fk_company_type_company"))
	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	@OneToOne
	@JoinColumn(name = "ADDRESS_ID", foreignKey = @ForeignKey(name = "fk_address_company"))
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Column(name = "EMAIL", length = ColumnLength.MEDIUM_TEXT, nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "CONTACT_NUMBER", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	@Column(name = "STATUS", length = ColumnLength.TINY_TEXT, nullable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "RESOURCE_ID", length = ColumnLength.TINY_TEXT)
	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	@OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<AccountDetail> getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(Set<AccountDetail> accountDetails) {
		this.accountDetails = accountDetails;
	}

	@Column(name = "NAME", length = ColumnLength.SHORT_TEXT, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "TAX_ID", length = ColumnLength.SHORT_TEXT, nullable = false)
	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	@Column(name = "MC_NO", length = ColumnLength.SHORT_TEXT, nullable = false)
	public String getMcNo() {
		return mcNo;
	}

	public void setMcNo(String mcNo) {
		this.mcNo = mcNo;
	}

	@Column(name = "DOT_NO", length = ColumnLength.SHORT_TEXT, nullable = false)
	public String getDotNo() {
		return dotNo;
	}

	public void setDotNo(String dotNo) {
		this.dotNo = dotNo;
	}

	@OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Trailer> getTrailers() {
		return trailers;
	}

	public void setTrailers(Set<Trailer> trailers) {
		this.trailers = trailers;
	}

	@OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Truck> getTrucks() {
		return trucks;
	}

	public void setTrucks(Set<Truck> trucks) {
		this.trucks = trucks;
	}

	@OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<Cargos> getCargos() {
		return cargos;
	}

	public void setCargos(Set<Cargos> cargos) {
		this.cargos = cargos;
	}
	
	

}
